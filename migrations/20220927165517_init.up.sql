CREATE TYPE "operator_type" AS ENUM (
  'beeline',
  'megafon',
  'mts',
  'tele2',
  'regular',
  'empty'
);

CREATE TYPE "send_types" AS ENUM (
  'direct',
  'kopilka'
);

CREATE TABLE "user" (
  "id" uuid PRIMARY KEY,
  "name" varchar NOT NULL,
  "login" varchar UNIQUE NOT NULL,
  "pass" varchar NOT NULL,
  "created_at" timestamp NOT NULL,
  "modified_at" timestamp NOT NULL
);

CREATE TABLE "lead" (
  "id" uuid PRIMARY KEY,
  "phone" varchar NOT NULL,
  "jk_id" integer,
  "jk_name" varchar,
  "jk_desc" varchar,
  "operator" operator_type,
  "city_id" integer,
  "pipeline_id" integer,
  "status_id" integer,
  "send_type" send_types,
  "short" boolean,
  "tags" varchar[],
  "utm_medium" varchar,
  "utm_term" varchar,
  "utm_source" varchar,
  "utm_campaign" varchar,
  "processed" boolean NOT NULL,
  "response_id" integer,
  "created_by" uuid NOT NULL,
  "created_at" timestamp NOT NULL,
  "modified_at" timestamp NOT NULL
);

CREATE TABLE "beeline_prefixes" (
  "id" uuid PRIMARY KEY,
  "prefix" integer NOT NULL,
  "jk_id" integer NOT NULL,
  "created_at" timestamp NOT NULL,
  "updated_at" timestamp NOT NULL
);

CREATE TABLE "amo_statuses" (
  "id" uuid PRIMARY KEY,
  "pipeline_name" varchar NOT NULL,
  "pipeline_id" integer NOT NULL,
  "status_name" varchar NOT NULL,
  "status_id" integer NOT NULL,
  "created_at" timestamp NOT NULL,
  "modified_at" timestamp NOT NULL
);

CREATE TABLE "city" (
  "id" integer PRIMARY KEY,
  "name" varchar NOT NULL,
  "created_at" timestamp NOT NULL,
  "modified_at" timestamp NOT NULL
);

ALTER TABLE "lead" ADD FOREIGN KEY ("created_by") REFERENCES "user" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;