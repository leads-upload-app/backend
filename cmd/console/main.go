package main

import (
	"amogo/internal/config"
	"amogo/internal/domain/user"
	"amogo/internal/repository"
	"amogo/internal/service"
	"amogo/pkg/amocrmClient"
	"amogo/pkg/amocrmClient/token"
	"amogo/pkg/gnApi"
	"context"
	"flag"
	"fmt"
	"os"
	"time"

	lg "gitlab.com/kanya384/gotools/logger"

	"gitlab.com/kanya384/gotools/psql"
)

var (
	name  string
	login string
	pass  string
)

const (
	tokenFilePath = "./vars/token_info.json"
)

func main() {
	flag.StringVar(&name, "name", "", "new users name")
	flag.StringVar(&login, "login", "", "new login")
	flag.StringVar(&pass, "pass", "", "new password")
	flag.Parse()

	if name == "" {
		fmt.Print("name not provided")
		os.Exit(1)
	}

	if login == "" {
		fmt.Print("login not provided")
		os.Exit(1)
	}
	if pass == "" {
		fmt.Print("pass not provided")
		os.Exit(1)
	}

	services := initApp()
	user, err := user.New(name, login, pass)
	if err != nil {
		fmt.Print("user error")
		os.Exit(1)
	}

	err = services.Auth.SignUp(context.Background(), user)

	if err != nil {
		fmt.Printf("user create error: %s", err.Error())
	}

	fmt.Println("user successfully created")
}

func initApp() (services *service.Service) {
	//read config
	cfg, err := config.InitConfig("")
	if err != nil {
		panic(fmt.Sprintf("error initializing config %s", err))
	}

	//setup logger
	logger, err := lg.New(cfg.Log.Level, cfg.App.ServiceName)
	if err != nil {
		panic(fmt.Sprintf("error initializing logger %s", err))
	}
	logFile, err := os.OpenFile(cfg.Log.Path, os.O_WRONLY|os.O_CREATE|os.O_APPEND, 0755)
	if err != nil {
		panic(fmt.Sprintf("error opening log file %s", err.Error()))
	}
	logger.SetOutput(logFile)

	//db init
	pg, err := psql.New(cfg.PG.Host, cfg.PG.Port, cfg.PG.DbName, cfg.PG.User, cfg.PG.Pass, psql.MaxPoolSize(cfg.PG.PoolMax), psql.ConnTimeout(time.Duration(cfg.PG.Timeout)*time.Second))
	if err != nil {
		panic(fmt.Sprintf("postgres connection error: %s", err.Error()))
	}

	//repository
	repository, err := repository.NewRepository(pg)
	if err != nil {
		panic(fmt.Sprintf("storage initialization error: %s", err.Error()))
	}

	//services
	portalApi := gnApi.New(cfg.Portal.Url, cfg.Portal.Key)
	token, err := token.ParseFromFile(tokenFilePath)
	if err != nil {
		logger.Fatal("error reading token file: %s", err.Error())
	}
	client := amocrmClient.NewAmoClient(cfg.Amo.ClientId, cfg.Amo.ClientSecret, cfg.Amo.RedirectUri, token)
	services, err = service.NewService(repository, cfg.Token.Secret, client, cfg.PassSalt, logger, portalApi)
	if err != nil {
		panic(fmt.Sprintf("services initialization error: %s", err.Error()))
	}
	return
}
