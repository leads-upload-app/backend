package gnApi

type City struct {
	ID   string `json:"ID"`
	Name string `json:"NAME"`
	Code string `json:"CODE"`
	SID  string `json:"SID"`
}

type JkUtmInfo struct {
	Id               string `json:"id"`
	Name             string `json:"name"`
	SId              string `json:"S_ID"`
	City             string `json:"CITY"`
	Description      string `json:"DESC"`
	UtmSourcePostav  string `json:"UTM_SOURCE_POSTAV"`
	UtmSourceTele2   string `json:"UTM_SOURCE_TELE2_JK"`
	UtmSourceMts     string `json:"UTM_SOURCE_MTS_JK"`
	UtmSourceBeeline string `json:"UTM_SOURCE_BEELINE_JK"`
	UtmSourceMegafon string `json:"UTM_SOURCE_MEGAFON_JK"`
}
