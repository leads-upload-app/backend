package gnApi

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
)

const (
	citiyListMethod   = "GetSites"
	jkParamsGetMethod = "GetUtmParams"
)

type GnApi struct {
	url string
	key string
}

func New(url string, key string) *GnApi {
	return &GnApi{
		url: url,
		key: key,
	}
}

func (p *GnApi) GetCityList(ctx context.Context) (cityList []*City, err error) {
	resp, err := http.Get(fmt.Sprintf("%s/?key=%s&met=%s", p.url, p.key, citiyListMethod))
	if err != nil {
		return nil, err
	}

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	cityList = []*City{}
	err = json.Unmarshal(body, &cityList)

	return
}

func (p *GnApi) GetJkParams(ctx context.Context, jkList []int) (jkParamsList map[string]JkUtmInfo, err error) {
	if len(jkList) == 0 {
		return nil, errors.New("no jks provided")
	}
	jkListString := ""
	for _, jkId := range jkList {
		jkListString += fmt.Sprintf("&id[]=%d", jkId)
	}
	resp, err := http.Get(fmt.Sprintf("%s/?key=%s&met=%s%s", p.url, p.key, jkParamsGetMethod, jkListString))
	if err != nil {
		return nil, err
	}

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	jkParamsList = map[string]JkUtmInfo{}
	err = json.Unmarshal(body, &jkParamsList)
	return
}
