package kopilka

import (
	"amogo/internal/domain/dmpLead"
	"amogo/internal/domain/lead"
	"strconv"
)

func (k *Kopilka) toKopilkaLead(lead *lead.Lead) (*SendLead, error) {
	tags := []string{}

	tags = append(tags, lead.Tags()...)

	if lead.IsJkFile() {
		for i := 0; i < len(tags); i++ {
			if tags[i] != "" {
				tags[i] += "_jk"
			}
		}
	}

	switch lead.Operator() {
	case "beeline":
		tags = append(tags, "Lead_Beeline")
	case "tele2":
		tags = append(tags, "Lead_Tele2")
	case "mts":
		tags = append(tags, "Lead_MTS")
	case "megafon":
		tags = append(tags, "Lead_Megafon")
	}

	utmContent := ""
	if lead.JkId() == 0 {
		utmContent = "1ekran"
	}
	return &SendLead{
		Phone:       lead.Phone(),
		IsBase:      "y",
		NoOrders:    "y",
		CityName:    "", //TODO get
		CityId:      lead.CityId(),
		UtType:      "http://leads.leadactiv.ru",
		PipelineId:  lead.PipelineId(),
		StatusId:    lead.StatusId(),
		UtmMedium:   lead.UtmMedium(),
		UtmSource:   lead.UtmSource(),
		UtmTerm:     lead.UtmTerm(),
		UtmCampaign: lead.UtmCampaign(),
		UtmContent:  utmContent,
		Tags:        tags,
		IdJk:        lead.JkId(),
		JkName:      lead.JkName(),
	}, nil
}

func (k *Kopilka) toKopilkaDmpLead(lead *dmpLead.DmpLead) (*SendLead, error) {
	tags := []string{}

	tags = append(tags, lead.Tag())

	utmContent := ""
	if lead.JkId() == 0 {
		utmContent = "1ekran"
	}

	cityId, err := strconv.Atoi(lead.CityId())
	if err != nil {
		return nil, err
	}

	return &SendLead{
		Phone:       lead.Phone(),
		IsBase:      "y",
		NoOrders:    "y",
		CityName:    "", //TODO get
		CityId:      cityId,
		UtType:      lead.Site(),
		PipelineId:  4444414,  //Проект по жк
		StatusId:    41190232, //Поступил лид
		UtmMedium:   lead.UtmMedium(),
		UtmSource:   lead.UtmSource(),
		UtmTerm:     lead.UtmTerm(),
		UtmCampaign: lead.UtmCampaign(),
		UtmContent:  utmContent,
		Tags:        tags,
		IdJk:        lead.JkId(),
		JkName:      lead.JkName(),
	}, nil
}
