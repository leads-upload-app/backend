package kopilka

import (
	"amogo/internal/domain/dmpLead"
	"amogo/internal/domain/lead"
	"fmt"
	"io"
	"net/http"
	"strconv"

	"github.com/ajg/form"
)

const (
	url = "https://api.g-n.ru/local/ajax/?lp_suns=y"
)

type Kopilka struct {
}

func NewKopilka() *Kopilka {
	return &Kopilka{}
}

type SendLead struct {
	Phone       string   `form:"phone"`
	IsBase      string   `form:"is_base"`
	NoOrders    string   `form:"no_orders"`
	CityName    string   `form:"s_name"`
	CityId      int      `form:"s_id"`
	UtType      string   `form:"ut_type"`
	PipelineId  int      `form:"pipeline_id"`
	StatusId    int      `form:"status_id"`
	UtmMedium   string   `form:"utm_medium"`
	UtmSource   string   `form:"utm_source"`
	UtmTerm     string   `form:"utm_term"`
	UtmCampaign string   `form:"utm_campaign"`
	UtmContent  string   `form:"utm_content"`
	Tags        []string `form:"amo_tags[]"`
	IdJk        int      `form:"jk_id"`
	JkName      string   `form:"jk_name"`
}

func (k *Kopilka) SendLead(lead *lead.Lead) (id int, err error) {
	sendLead, err := k.toKopilkaLead(lead)
	if err != nil {
		return
	}

	client := &http.Client{}

	data, err := form.EncodeToValues(sendLead)
	if err != nil {
		return
	}

	resp, err := client.PostForm(url, data)
	if err != nil {
		return
	}
	defer resp.Body.Close()

	response, err := io.ReadAll(resp.Body)
	if err != nil {
		return 0, fmt.Errorf("error parsing response from kopilka: %s", string(response))
	}

	if resp.StatusCode != 200 {
		return 0, fmt.Errorf("invalid status code from kopilka: %d", resp.StatusCode)
	}

	id, errC := strconv.Atoi(string(response))
	if errC != nil {
		id = 999999999
	}

	return
}

func (k *Kopilka) SendDmpOneLead(lead *dmpLead.DmpLead) (id int, err error) {
	sendLead, err := k.toKopilkaDmpLead(lead)
	if err != nil {
		return
	}

	client := &http.Client{}

	data, err := form.EncodeToValues(sendLead)
	if err != nil {
		return
	}

	resp, err := client.PostForm(url, data)
	if err != nil {
		return
	}
	defer resp.Body.Close()

	response, err := io.ReadAll(resp.Body)
	if err != nil {
		return 0, fmt.Errorf("error parsing response from kopilka: %s", string(response))
	}

	if resp.StatusCode != 200 {
		return 0, fmt.Errorf("invalid status code from kopilka: %d", resp.StatusCode)
	}

	id, errC := strconv.Atoi(string(response))
	if errC != nil {
		id = 999999999
	}

	return
}
