package excel

import (
	"io"
	"strconv"

	"github.com/xuri/excelize/v2"
)

type Excel interface {
	CreateExcelFile(filename string, data [][]string) (err error)
	ParseFile(fileReader io.Reader) (rows [][]string, err error)
}

type excel struct {
}

func NewExcelService() Excel {
	return &excel{}
}

func (e *excel) CreateExcelFile(filename string, data [][]string) (err error) {
	f := excelize.NewFile()
	for rowIndex, values := range data {
		for cellIndex, value := range values {
			f.SetCellValue("Sheet1", toChar(cellIndex+1)+strconv.Itoa(rowIndex+1), value)
		}
	}

	return f.SaveAs(filename)
}

func (e *excel) ParseFile(fileReader io.Reader) (rows [][]string, err error) {
	file, err := excelize.OpenReader(fileReader)
	if err != nil {
		return
	}

	sheets := file.GetSheetList()
	for _, sheet := range sheets {
		rows, err = file.GetRows(sheet)
		if err != nil {
			return
		}
		if len(rows) != 0 {
			return rows, nil
		}
	}

	return
}

func toChar(i int) string {
	return string(rune('A' - 1 + i))
}
