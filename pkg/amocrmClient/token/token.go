package token

import (
	"encoding/json"
	"io"
	"os"
)

type Token struct {
	accessToken  string
	expires      int
	refreshToken string
	baseDomain   string
	filePath     string
}

func NewToken(accessToken string, expires int, refreshToken string, baseDomain string) *Token {
	return &Token{
		accessToken:  accessToken,
		expires:      expires,
		refreshToken: refreshToken,
		baseDomain:   baseDomain,
	}
}

func ParseFromFile(filePath string) (token *Token, err error) {
	tokenInput := fileStruct{}
	file, _ := os.Open(filePath)
	defer file.Close()

	data, err := io.ReadAll(file)
	if err != nil {
		return nil, err
	}

	err = json.Unmarshal(data, &tokenInput)
	if err != nil {
		return nil, err
	}

	return &Token{
		accessToken:  tokenInput.AccessToken,
		expires:      tokenInput.Expires,
		refreshToken: tokenInput.RefreshToken,
		baseDomain:   tokenInput.BaseDomain,
		filePath:     filePath,
	}, nil
}

func (t *Token) StoreToFile() (err error) {
	tokenOutput := fileStruct{
		AccessToken:  t.accessToken,
		Expires:      t.expires,
		RefreshToken: t.refreshToken,
		BaseDomain:   t.baseDomain,
	}

	data, err := json.Marshal(tokenOutput)
	if err != nil {
		return err
	}

	return os.WriteFile(t.filePath, data, 0644)
}

func (t *Token) Token() string {
	return t.accessToken
}

func (t *Token) SetToken(accessToken string) {
	t.accessToken = accessToken
}

func (t *Token) Expires() int {
	return t.expires
}

func (t *Token) SetExpires(expires int) {
	t.expires = expires
}

func (t *Token) RefreshToken() string {
	return t.refreshToken
}

func (t *Token) SetRefreshToken(refreshToken string) {
	t.refreshToken = refreshToken
}

func (t *Token) BaseDomain() string {
	return t.baseDomain
}
