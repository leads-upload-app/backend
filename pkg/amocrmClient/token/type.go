package token

type fileStruct struct {
	AccessToken  string `json:"accessToken"`
	Expires      int    `json:"expires"`
	RefreshToken string `json:"refreshToken"`
	BaseDomain   string `json:"baseDomain"`
}
