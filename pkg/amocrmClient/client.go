package amocrmClient

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
	"time"

	"amogo/pkg/amocrmClient/token"
)

const (
	sendLeadComplex  = "/api/v4/leads/complex"
	getPipelinesList = "/api/v4/leads/pipelines"
	refreshPath      = "/oauth2/access_token"
)

type Client struct {
	token        *token.Token
	clientId     string
	clientSecret string
	redirectUri  string
}

func NewAmoClient(clientId string, clientSecret string, redirectUri string, token *token.Token) *Client {
	return &Client{
		token:        token,
		clientId:     clientId,
		clientSecret: clientSecret,
		redirectUri:  redirectUri,
	}
}

func (c *Client) RefreshToken() (err error) {
	requestRefreshData := RefreshTokenRequest{
		ClientId:     c.clientId,
		ClientSecret: c.clientSecret,
		GrantType:    "refresh_token",
		RefreshToken: c.token.RefreshToken(),
		RedirectUri:  c.redirectUri,
	}
	response := &RefreshTokenResponse{}
	_, err = c.sendRequest(fmt.Sprintf("https://%s%s", c.token.BaseDomain(), refreshPath), http.MethodPost, "application/json", requestRefreshData, response)
	if err != nil {
		return
	}

	c.token.SetExpires(response.ExpiresIn)
	c.token.SetToken(response.AccessToken)
	c.token.SetRefreshToken(response.RefreshToken)
	c.token.StoreToFile()
	return nil
}

func (c *Client) SendLeadComplex(params SendLeadComplexRequest) (result SendLeadComplexResponse, err error) {
	start := time.Now()

	response := []*AmoLeadComplexResponse{}

	code, err := c.sendRequest(fmt.Sprintf("https://%s%s", c.token.BaseDomain(), sendLeadComplex), http.MethodPost, "application/json", params.Leads, &response)
	if err != nil {
		return
	}
	if code == 401 {
		err = errors.New("unauthorized, check token")
		c.RefreshToken()
		return
	}
	if code >= 400 {
		err = errors.New("unhandled error")
		return
	}

	return SendLeadComplexResponse{
		Result:   response,
		Code:     code,
		Duration: time.Since(start),
	}, err
}

func (c *Client) GetPipelinesList() (pipelines []*Pipeline, err error) {
	reponse := PipelinesListAmoResponse{}
	code, err := c.sendRequest(fmt.Sprintf("https://%s%s", c.token.BaseDomain(), getPipelinesList), http.MethodGet, "application/json", nil, &reponse)
	if err != nil {
		return
	}
	if code == 401 {
		err = errors.New("unauthorized, check token")
		c.RefreshToken()
		return
	}
	if code >= 400 {
		err = errors.New("unhandled error")
		return
	}
	return reponse.Embeeded.Pipelines, nil
}

func (c *Client) sendRequest(url, method, contentType string, data interface{}, response interface{}) (code int, err error) {
	jsonData, err := json.Marshal(data)
	if err != nil {
		return
	}

	req, err := http.NewRequest(method, url, bytes.NewBuffer(jsonData))
	if err != nil {
		return 0, err
	}

	req.Header.Set("Content-Type", contentType)
	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", c.token.Token()))

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return 0, err
	}

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return 0, fmt.Errorf("no body in response")
	}

	err = json.Unmarshal(body, response)
	if err != nil {
		return 0, fmt.Errorf("error decode response from %s - error %s, body of response: %s", url, err.Error(), string(body))
	}

	return resp.StatusCode, err
}
