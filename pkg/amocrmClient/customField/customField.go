package customField

type CustomField struct {
	FieldId int    `json:"field_id"`
	Values  []Enum `json:"values"`
}
