package contact

import "amogo/pkg/amocrmClient/contact/customField"

type Contact struct {
	FirstName         string                    `json:"first_name"`
	CustomFieldValues []customField.CustomField `json:"custom_fields_values"`
}
