package amocrmClient

import (
	"amogo/pkg/amocrmClient/lead"
	"time"
)

type SendLeadComplexRequest struct {
	Leads []*lead.Lead
}

type SendLeadComplexResponse struct {
	Code     int
	Result   []*AmoLeadComplexResponse
	Duration time.Duration
}

type AmoLeadComplexResponse struct {
	LeadId    int      `json:"id"`
	ContactId int      `json:"contact_id"`
	RequestId []string `json:"request_id"`
	Merged    bool     `json:"merged"`
}

type PipelinesListAmoResponse struct {
	Embeeded PipelinesListAmoResponseEmbeeded `json:"_embedded"`
}

type PipelinesListAmoResponseEmbeeded struct {
	Pipelines []*Pipeline `json:"pipelines"`
}

type Pipeline struct {
	Id        int              `json:"id"`
	Name      string           `json:"name"`
	IsMain    bool             `json:"is_main"`
	IsArchive bool             `json:"is_archive"`
	Embeeded  PipelineEmbedded `json:"_embedded"`
}

type PipelineEmbedded struct {
	Statuses []Status `json:"statuses"`
}

type Status struct {
	Id         int    `json:"id"`
	Name       string `json:"name"`
	PipelineId int    `json:"pipeline_id"`
}

type RefreshTokenRequest struct {
	ClientId     string `json:"client_id"`
	ClientSecret string `json:"client_secret"`
	GrantType    string `json:"grant_type"`
	RefreshToken string `json:"refresh_token"`
	RedirectUri  string `json:"redirect_uri"`
}

type RefreshTokenResponse struct {
	TokenType    string `json:"token_type"`
	ExpiresIn    int    `json:"expires_in"`
	AccessToken  string `json:"access_token"`
	RefreshToken string `json:"refresh_token"`
}
