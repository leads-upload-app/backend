package embeeded

import (
	"amogo/pkg/amocrmClient/contact"
	"amogo/pkg/amocrmClient/tag"
)

type Embedded struct {
	Contacts []*contact.Contact `json:"contacts"`
	Tags     []*tag.Tag         `json:"tags"`
}
