#---Build stage---
FROM golang:1.19 AS builder
COPY . /go/src/
WORKDIR /go/src/cmd/app

RUN GOOS=linux GOARCH=amd64 CGO_ENABLED=0 go build -ldflags='-w -s' -o /go/bin/service

WORKDIR /go/src/cmd/console
RUN GOOS=linux GOARCH=amd64 CGO_ENABLED=0 go build -ldflags='-w -s' -o /go/bin/console
#---Final stage---
FROM alpine:latest
COPY --from=builder /go/bin/service /go/bin/service
COPY --from=builder /go/src/migrations /migrations
COPY --from=builder /go/bin/console /go/bin/console
CMD /go/bin/service --port 8090 --host '0.0.0.0'