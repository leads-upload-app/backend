package delivery

import (
	"context"
	"net/http"

	"github.com/gin-gonic/gin"

	jsonCity "amogo/internal/delivery/city"
)

// CityList
// @Summary Возвращает список  городов портала.
// @Description Возвращает список  городов портала.
// @Security ApiKeyAuth
// @Tags city
// @Accept  json
// @Produce json
// @Success 200			{object}  	jsonCity.CityListResponse
// @Failure 400 		{object}    ErrorResponse
// @Failure 401 		{object}    ErrorResponse
// @Router /city/list [get]
func (d *Delivery) CityList(c *gin.Context) {
	list, err := d.services.City.ReadCityList(context.Background())
	if err != nil {
		SetError(c, http.StatusInternalServerError, err.Error())
		return
	}
	resultList := []*jsonCity.City{}
	for _, st := range list {
		resultList = append(resultList, jsonCity.ToResponseCity(st))
	}
	c.JSON(http.StatusOK, jsonCity.CityListResponse{List: resultList})
}
