package delivery

import (
	jsonLead "amogo/internal/delivery/dmpOne"
	"amogo/internal/domain/dmpLead/filter"
	"context"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

// CrateLeadDmpOne
// @Summary Создание лида dmp.one.
// @Description Создание лида dmp.one.
// @Tags dmpone
// @Accept  multipart/form-data
// @Param        id   path      int  true  "Идентификатор жк"
// @Param body formData jsonLead.CreateLeadDmpOne true "Body"
// @Produce json
// @Success 201
// @Failure 400 		{object}    ErrorResponse
// @Failure 401 		{object}    ErrorResponse
// @Router /dmpone/{id} [post]
func (d *Delivery) CrateLeadDmpOne(c *gin.Context) {
	id := c.Param("id")
	jkId, err := strconv.Atoi(id)
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	jsonLeadInput := &jsonLead.CreateLeadDmpOne{}

	if err := c.ShouldBind(&jsonLeadInput); err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	dmpLead, err := jsonLead.ToDomainLead(jsonLeadInput, jkId)
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	err = d.services.DmpLeads.CreateLead(context.Background(), dmpLead)
	if err != nil {
		SetError(c, http.StatusInternalServerError, err.Error())
		return
	}

	c.Status(http.StatusCreated)
}

// GetDmpLeadsList
// @Summary Получение списка лидов.
// @Description Получение списка лидов.
// @Tags dmpone
// @Accept json
// @Produce json
// @Param filter body jsonLead.Filter true "Body"
// @Success 200		{object} 	jsonLead.GetLeadsListResponse
// @Failure 400 		{object}    ErrorResponse
// @Failure 401 		{object}    ErrorResponse
// @Security ApiKeyAuth
// @Router /dmpone/list [post]
func (d *Delivery) GetDmpLeadsList(c *gin.Context) {
	form := &jsonLead.Filter{}
	if err := c.ShouldBind(&form); err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	ctx, cancel := context.WithTimeout(context.Background(), createLeadsTimeOut)
	defer cancel()

	filter := filter.Filter{
		DateStart: form.DateStart,
		DateEnd:   form.DateEnd,
		JkId:      form.JkId,
	}

	leads, err := d.services.DmpLeads.ReadLeadsFiltredList(ctx, filter)
	if err != nil {
		SetError(c, http.StatusInternalServerError, err.Error())
		return
	}

	responseLeads := []*jsonLead.DmpOneLeadResponse{}
	for _, lead := range leads {
		leadR, err := jsonLead.ToResponseLead(lead)
		if err != nil {
			SetError(c, http.StatusInternalServerError, err.Error())
			return
		}
		responseLeads = append(responseLeads, leadR)
	}

	c.JSON(http.StatusOK, jsonLead.GetLeadsListResponse{
		Leads: responseLeads,
	})
}
