package delivery

import (
	docs "amogo/internal/delivery/swagger/docs"

	"github.com/gin-gonic/gin"
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
)

// @title golang amo service
// @version 1.0
// @description golang amo service
// @license.name kanya384

// @contact.name API Support
// @contact.email kanya384@mail.ru

// @BasePath /
// @securityDefinitions.apikey ApiKeyAuth
// @in header
// @name Authorization
func (d *Delivery) initRouter() *gin.Engine {

	var router = gin.New()

	d.routerDocs(router.Group("/docs"))
	d.routerAuth(router.Group("/auth"))
	router.POST("/dmpone/:id", d.CrateLeadDmpOne)

	router.Use(d.checkAuth)
	d.routerDmp(router.Group("/dmpone"))

	d.routerAmo(router.Group("/amo"))
	d.routerCity(router.Group("/city"))
	d.routerBeeline(router.Group("/beeline"))
	d.routerLeadsList(router.Group("/leads"))

	return router
}

func (d *Delivery) routerAuth(router *gin.RouterGroup) {
	router.POST("/sign-in", d.SignIn)
}

func (d *Delivery) routerDmp(router *gin.RouterGroup) {
	router.POST("/list", d.GetDmpLeadsList)
}

func (d *Delivery) routerAmo(router *gin.RouterGroup) {
	router.GET("/status", d.AmoStatusList)
}

func (d *Delivery) routerCity(router *gin.RouterGroup) {
	router.GET("/list", d.CityList)
}

func (d *Delivery) routerBeeline(router *gin.RouterGroup) {
	router.GET("/prefix", d.BeelinePrefixList)
	router.PUT("/upload", d.BeelineUploadFile)
}

func (d *Delivery) routerLeadsList(router *gin.RouterGroup) {
	router.PUT("/list", d.CrateLeadsList)
	router.POST("/list", d.GetLeadsList)
	router.GET("/count", d.GetUnsendedLeadsCount)
}

func (d *Delivery) routerDocs(router *gin.RouterGroup) {
	docs.SwaggerInfo.BasePath = "/"

	router.Any("/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))
}
