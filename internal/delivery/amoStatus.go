package delivery

import (
	"context"
	"net/http"

	jsonAmoStatus "amogo/internal/delivery/amoStatus"

	"github.com/gin-gonic/gin"
)

// AmoStatusList
// @Summary Возвращает список статусов пайплайнов.
// @Description Возвращает список статусов пайплайнов.
// @Security ApiKeyAuth
// @Tags amo
// @Accept  json
// @Produce json
// @Success 200			{object}  	jsonAmoStatus.AmoStatusListResponse
// @Failure 400 		{object}    ErrorResponse
// @Failure 401 		{object}    ErrorResponse
// @Router /amo/status [get]
func (d *Delivery) AmoStatusList(c *gin.Context) {
	list, err := d.services.AmoStatus.ReadAmoStatusList(context.Background())
	if err != nil {
		SetError(c, http.StatusInternalServerError, err.Error())
		return
	}
	resultList := []*jsonAmoStatus.Status{}
	for _, st := range list {
		resultList = append(resultList, jsonAmoStatus.ToResponseAmoStatus(st))
	}
	c.JSON(http.StatusOK, jsonAmoStatus.AmoStatusListResponse{List: resultList})
}
