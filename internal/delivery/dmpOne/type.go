package dmpOne

import (
	"time"

	"github.com/google/uuid"
)

type CreateLeadDmpOne struct {
	Phone          string   `form:"phone" example:"71234567890"`
	Page           string   `form:"page" example:"dmp.one"`
	PageWithParams string   `form:"page_with_parameters" example:"dmp.one"`
	Email          []string `form:"email[]" example:"[test@test.test]"`
	UtmMedium      string   `form:"utm_medium"`
	UtmTerm        string   `form:"utm_term"`
	UtmSource      string   `form:"utm_source"`
	UtmCampaign    string   `form:"utm_campaign"`
	UtmContent     string   `form:"utm_content"`
	Roistat        string   `form:"roistat"`
	YId            string   `form:"yid"`
}

type GetLeadsListResponse struct {
	Leads []*DmpOneLeadResponse `json:"leads"  binding:"required"`
}

type DmpOneLeadResponse struct {
	Id          uuid.UUID `json:"id"  binding:"required"`
	Phone       string    `json:"phone" binding:"required"`
	Site        string    `json:"site" binding:"required"`
	Email       string    `json:"email" binding:"required"`
	UtmMedium   string    `json:"utmMedium" binding:"required"`
	UtmTerm     string    `json:"utmTerm" binding:"required"`
	UtmSource   string    `json:"utmSource" binding:"required"`
	UtmCampaign string    `json:"utmCampaign" binding:"required"`
	UtmContent  string    `json:"utmContent" binding:"required"`
	Tag         string    `json:"tag" example:"tag"`
	Roistat     string    `json:"roistat" binding:"required"`
	Yid         string    `json:"yId" binding:"required"`
	JkId        int       `json:"jkId" binding:"required"`
	JkName      string    `json:"jkName"`
	CityId      string    `json:"cityId"`
	Processed   bool      `json:"processed"`
	ResponseId  int       `json:"responseId"`
	CreatedAt   time.Time `json:"createdAt" binding:"required"`
	ModifiedAt  time.Time `json:"modifiedAt" binding:"required"`
}

type Filter struct {
	DateStart time.Time `json:"dateStart"  binding:"required" example:"2023-01-17T00:00:00Z"`
	DateEnd   time.Time `json:"dateEnd"  binding:"required" example:"2023-01-17T00:00:00Z"`
	JkId      int       `json:"jk_id" example:"123456"`
}
