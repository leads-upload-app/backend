package dmpOne

import (
	dOneLead "amogo/internal/domain/dmpLead"
)

func ToDomainLead(lead *CreateLeadDmpOne, jkId int) (*dOneLead.DmpLead, error) {
	email := ""
	if len(lead.Email) > 0 {
		email = lead.Email[0]
	}
	return dOneLead.New(lead.Phone, lead.Page, lead.Page, email, "", lead.UtmMedium, lead.UtmTerm, lead.UtmSource, lead.UtmCampaign, lead.UtmContent, lead.Roistat, lead.YId, jkId, "", "")
}

func ToResponseLead(lead *dOneLead.DmpLead) (*DmpOneLeadResponse, error) {
	return &DmpOneLeadResponse{
		Id:          lead.Id(),
		Phone:       lead.Phone(),
		Site:        lead.Site(),
		JkId:        lead.JkId(),
		JkName:      lead.JkName(),
		CityId:      lead.CityId(),
		Email:       lead.Email(),
		UtmMedium:   lead.UtmMedium(),
		UtmTerm:     lead.UtmTerm(),
		UtmSource:   lead.UtmSource(),
		UtmCampaign: lead.UtmCampaign(),
		UtmContent:  lead.UtmContent(),
		Roistat:     lead.Roistat(),
		Tag:         lead.Tag(),
		Yid:         lead.Yid(),
		Processed:   lead.Processed(),
		ResponseId:  lead.ResponseId(),
		CreatedAt:   lead.CreatedAt(),
		ModifiedAt:  lead.ModifiedAt(),
	}, nil
}
