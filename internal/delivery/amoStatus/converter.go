package amoStatus

import "amogo/internal/domain/amoStatus"

func ToResponseAmoStatus(amoStatus *amoStatus.AmoStatus) (status *Status) {
	return &Status{
		PipelineName: amoStatus.PipelineName(),
		PipelineId:   amoStatus.PipelineId(),
		StatusName:   amoStatus.StatusName(),
		StatusId:     amoStatus.StatusId(),
	}
}
