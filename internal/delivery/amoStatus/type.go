package amoStatus

type AmoStatusListResponse struct {
	List []*Status `json:"list"  binding:"required"`
}
type Status struct {
	PipelineName string `json:"pipelineName"  binding:"required"`
	PipelineId   int    `json:"pipelineId"  binding:"required"`
	StatusName   string `json:"statusName"  binding:"required"`
	StatusId     int    `json:"statusId"  binding:"required"`
}
