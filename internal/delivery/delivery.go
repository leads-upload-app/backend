package delivery

import (
	"amogo/internal/service"
	"errors"
	"fmt"
	"net/http"
	"strings"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"gitlab.com/kanya384/gotools/auth"
)

type Delivery struct {
	services *service.Service
	router   *gin.Engine
	tm       auth.TokenManager

	options Options
}

type Options struct{}

func New(services *service.Service, signingKey string, options Options) (*Delivery, error) {
	tm, err := auth.NewManager(signingKey)
	if err != nil {
		return nil, err
	}
	var d = &Delivery{
		services: services,
		tm:       tm,
	}

	d.SetOptions(options)

	d.router = d.initRouter()
	return d, nil
}

func (d *Delivery) SetOptions(options Options) {
	if d.options != options {
		d.options = options
	}
}

func (d *Delivery) Run(port int) error {
	return d.router.Run(fmt.Sprintf(":%d", port))
}

func (d *Delivery) checkAuth(c *gin.Context) {
	token := c.GetHeader("Authorization")
	if token == "" || strings.Contains("Bearer ", token) {
		c.AbortWithStatusJSON(http.StatusUnauthorized, "please authorize")
		return
	}

	token = strings.Replace(token, "Bearer ", "", 1)

	result, err := d.tm.Parse(token)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusUnauthorized, "please authorize")
		return
	}
	id, _ := uuid.Parse(result.UserID)

	setAuthInfoToContext(c, &AuthParams{
		UserId:   id,
		UserName: result.UserName,
		UserRole: result.UserRole,
	})

	c.Next()
}

func setAuthInfoToContext(c *gin.Context, params *AuthParams) {
	c.Set("userId", params.UserId)
	c.Set("userName", params.UserName)
	c.Set("userRole", params.UserRole)
}

func getAuthInfoFromContext(c *gin.Context) (params *AuthParams, err error) {
	userId, ok := c.Get("userId")
	if !ok {
		return nil, errors.New("not valid auth")
	}

	userName, ok := c.Get("userName")
	if !ok {
		return nil, errors.New("not valid auth")
	}

	userRole, ok := c.Get("userRole")
	if !ok {
		return nil, errors.New("not valid auth")
	}

	return &AuthParams{
		UserId:   userId.(uuid.UUID),
		UserName: userName.(string),
		UserRole: userRole.(string),
	}, nil
}
