package delivery

import (
	jsonLead "amogo/internal/delivery/lead"
	"amogo/internal/domain/lead"
	"amogo/internal/domain/lead/filter"
	"context"
	"fmt"
	"mime/multipart"
	"net/http"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
)

const (
	createLeadsTimeOut = time.Second * 10
)

// GetLeadsList
// @Summary Получение списка лидов.
// @Description Получение списка лидов.
// @Tags leads
// @Accept json
// @Produce json
// @Param filter body jsonLead.Filter true "Body"
// @Success 200		{object} 	jsonLead.GetLeadsListResponse
// @Failure 400 		{object}    ErrorResponse
// @Failure 401 		{object}    ErrorResponse
// @Security ApiKeyAuth
// @Router /leads/list [post]
func (d *Delivery) GetLeadsList(c *gin.Context) {
	auth, err := getAuthInfoFromContext(c)
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}
	form := &jsonLead.Filter{}
	if err := c.ShouldBind(&form); err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}
	ctx, cancel := context.WithTimeout(context.Background(), createLeadsTimeOut)
	defer cancel()

	filter := filter.Filter{
		DateStart:   form.DateStart,
		DateEnd:     form.DateEnd,
		SendType:    lead.SendTypes(form.SendType),
		Tag:         form.Tag,
		UtmMedium:   form.UtmMedium,
		UtmTerm:     form.UtmTerm,
		UtmSource:   form.UtmSource,
		UtmCampaign: form.UtmCampaign,
		CreatedBy:   auth.UserId,
	}

	leads, err := d.services.Leads.GetLeadsList(ctx, filter)

	if err != nil {
		SetError(c, http.StatusInternalServerError, err.Error())
		return
	}

	responseLeads := []*jsonLead.LeadResponse{}
	for _, lead := range leads {
		leadR, err := jsonLead.ToResponseLead(lead)
		if err != nil {
			SetError(c, http.StatusInternalServerError, err.Error())
			return
		}
		responseLeads = append(responseLeads, leadR)
	}

	c.JSON(http.StatusOK, jsonLead.GetLeadsListResponse{
		Leads: responseLeads,
	})
}

// CrateLeadsList
// @Summary Загрузка списка лидов.
// @Description Загрузка списка лидов.
// @Tags leads
// @Accept  multipart/form-data
// @Param body formData jsonLead.CreateLeadListRequest true "Body"
// @Param file formData file false "File to upload"
// @Produce json
// @Success 201			{object} 	jsonLead.CreateLeadListResponse
// @Failure 400 		{object}    ErrorResponse
// @Failure 401 		{object}    ErrorResponse
// @Security ApiKeyAuth
// @Router /leads/list [put]
func (d *Delivery) CrateLeadsList(c *gin.Context) {
	auth, err := getAuthInfoFromContext(c)
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}
	form := &jsonLead.CreateLeadListRequest{}

	if err := c.ShouldBind(&form); err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	formFile, _ := c.FormFile("file")

	if len(form.Phones) == 0 && formFile == nil {
		SetError(c, http.StatusBadRequest, "file or phonelist must be provided")
		return
	}

	ctx, cancel := context.WithTimeout(context.Background(), createLeadsTimeOut)
	defer cancel()

	//swag not splits slices, doing it it manualy
	if len(form.Phones) > 0 {
		form.Phones = strings.Split(form.Phones[0], ",")
	}
	if len(form.Tags) > 0 {
		form.Tags = strings.Split(form.Tags[0], ",")
	}

	if form.Operator == "" {
		form.Operator = "empty"
	}

	var fileReader *multipart.File

	if formFile != nil {
		res, err := formFile.Open()
		if err != nil {
			SetError(c, http.StatusInternalServerError, err.Error())
			return
		}
		fileReader = &res
	}

	leadParams := jsonLead.ToCrateLeadProperties(form)
	leadParams.CreatedBy = auth.UserId

	leadsCount, err := d.services.Leads.CrateLeadsList(ctx, leadParams, fileReader)
	if err != nil {
		SetError(c, http.StatusInternalServerError, err.Error())
		return
	}

	c.JSON(http.StatusCreated, jsonLead.CreateLeadListResponse{
		Message: fmt.Sprintf("successfully created %d leads", leadsCount),
	})
}

// GetUnsendedLeadsCount
// @Summary Получить количество неотправленных лидов в копилке и буфере.
// @Description Получить количество неотправленных лидов в копилке и буфере.
// @Tags leads
// @Produce json
// @Success 201			{object} 	jsonLead.CountsResponse
// @Failure 400 		{object}    ErrorResponse
// @Failure 401 		{object}    ErrorResponse
// @Security ApiKeyAuth
// @Router /leads/count [get]
func (d *Delivery) GetUnsendedLeadsCount(c *gin.Context) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()

	direct, kopilka, err := d.services.Leads.GetUnsendedLeadsCount(ctx)
	if err != nil {
		SetError(c, http.StatusInternalServerError, err.Error())
		return
	}

	c.JSON(http.StatusCreated, jsonLead.CountsResponse{
		Direct:  direct,
		Kopilka: kopilka,
	})
}
