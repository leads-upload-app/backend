package lead

import (
	"mime/multipart"
	"time"

	"github.com/google/uuid"
)

type CreateLeadListRequest struct {
	Phones      []string              `form:"phones" example:"79000000000,79000000001"`
	JkId        int                   `form:"jkId" example:"324762"`
	JkName      string                `form:"jkName" example:"Жилой комплекс «Pride (Прайд)»"`
	JkDesc      string                `form:"jkDesc"`
	Operator    string                `form:"operator"`
	CityId      int                   `form:"cityId" binding:"required" example:"258828"`
	PipelineId  int                   `form:"pipelineId" binding:"required" example:"47172541"`
	StatusId    int                   `form:"statusId" binding:"required" example:"47172541"`
	SendType    string                `form:"sendType" binding:"required" example:"'direct' or 'kopilka'"`
	Short       bool                  `form:"short" example:"true"`
	Tags        []string              `form:"tags" example:"Beeline_Lead"`
	UtmMedium   string                `form:"utmMedium"`
	UtmTerm     string                `form:"utmTerm"`
	UtmSource   string                `form:"utmSource"`
	UtmCampaign string                `form:"utmCampaign"`
	IsJkFile    bool                  `form:"isJkFile"`
	File        *multipart.FileHeader `form:"file"`
}

type CreateLeadListResponse struct {
	Message string `json:"message"`
}

type GetLeadsListResponse struct {
	Leads []*LeadResponse `json:"leads"  binding:"required"`
}

type LeadResponse struct {
	Id          uuid.UUID `json:"id"  binding:"required"`
	Phone       string    `json:"phone" binding:"required"`
	JkId        int       `json:"jkId" binding:"required"`
	JkName      string    `json:"jkName" binding:"required"`
	JkDesc      string    `json:"jkDesc" binding:"required"`
	Operator    string    `json:"operator" binding:"required"`
	CityId      int       `json:"cityId" binding:"required"`
	PipelineId  int       `json:"pipelineId" binding:"required"`
	StatusId    int       `json:"statusId" binding:"required"`
	SendType    string    `json:"sendType" binding:"required"`
	Short       bool      `json:"bool" binding:"required"`
	Tags        []string  `json:"tags" binding:"required"`
	UtmMedium   string    `json:"utmMedium" binding:"required"`
	UtmTerm     string    `json:"utmTerm" binding:"required"`
	UtmSource   string    `json:"utmSource" binding:"required"`
	UtmCampaign string    `json:"utmCampaign" binding:"required"`
	IsJkFile    bool      `json:"isJkFile" binding:"required"`
	Processed   bool      `json:"processed" binding:"required"`
	ResponseId  int       `json:"responseId" binding:"required"`
	CreatedBy   uuid.UUID `json:"createdBy" binding:"required"`
	CreatedAt   time.Time `json:"createdAt" binding:"required"`
	ModifiedAt  time.Time `json:"modifiedAt" binding:"required"`
}

type Filter struct {
	SendType    string    `json:"sendType"`
	Tag         string    `json:"tag"`
	UtmMedium   string    `json:"utmMedium"`
	UtmTerm     string    `json:"utmTerm"`
	UtmSource   string    `json:"utmSource"`
	UtmCampaign string    `json:"utmCampaign"`
	CreatedBy   uuid.UUID `json:"createdBy"`
	DateStart   time.Time `json:"dateStart"  binding:"required" example:"2023-01-17T00:00:00Z"`
	DateEnd     time.Time `json:"dateEnd"  binding:"required" example:"2023-01-17T23:59:59Z"`
}

type CountsResponse struct {
	Direct  int `json:"direct"`
	Kopilka int `json:"kopilka"`
}
