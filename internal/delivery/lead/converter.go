package lead

import (
	dLead "amogo/internal/domain/lead"
	lead "amogo/internal/service/lead"
)

func ToCrateLeadProperties(params *CreateLeadListRequest) *lead.CreateParams {
	return &lead.CreateParams{
		Phones:      params.Phones,
		JkId:        params.JkId,
		JkName:      params.JkName,
		JkDesc:      params.JkDesc,
		Operator:    params.Operator,
		CityId:      params.CityId,
		PipelineId:  params.PipelineId,
		StatusId:    params.StatusId,
		SendType:    params.SendType,
		Short:       params.Short,
		Tags:        params.Tags,
		UtmMedium:   params.UtmMedium,
		UtmTerm:     params.UtmTerm,
		UtmSource:   params.UtmSource,
		UtmCampaign: params.UtmCampaign,
		IsJkFile:    params.IsJkFile,
	}
}

func ToResponseLead(lead *dLead.Lead) (*LeadResponse, error) {
	return &LeadResponse{
		Id:          lead.Id(),
		Phone:       lead.Phone(),
		JkId:        lead.JkId(),
		JkName:      lead.JkName(),
		Operator:    string(lead.Operator()),
		CityId:      lead.CityId(),
		PipelineId:  lead.PipelineId(),
		StatusId:    lead.StatusId(),
		SendType:    string(lead.SendType()),
		Short:       lead.IsShort(),
		Tags:        lead.Tags(),
		UtmMedium:   lead.UtmMedium(),
		UtmTerm:     lead.UtmTerm(),
		UtmSource:   lead.UtmSource(),
		UtmCampaign: lead.UtmCampaign(),
		Processed:   lead.Processed(),
		ResponseId:  lead.ResponseId(),
		CreatedBy:   lead.CreatedBy(),
		CreatedAt:   lead.CreatedAt(),
		ModifiedAt:  lead.ModifiedAt(),
		IsJkFile:    lead.IsJkFile(),
	}, nil
}
