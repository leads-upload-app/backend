package beelinePrefixes

type PrefxListResponse struct {
	List []*BeelinePrefix `json:"list"  binding:"required"`
}
type BeelinePrefix struct {
	Prefix int `json:"prefix"  binding:"required"`
	JkId   int `json:"jkId"  binding:"required"`
}
