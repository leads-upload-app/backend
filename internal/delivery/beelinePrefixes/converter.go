package beelinePrefixes

import (
	beelinePrefix "amogo/internal/domain/beelinePrefixes"
)

func ToResponsePrefix(prefix *beelinePrefix.BeelinePrefix) *BeelinePrefix {
	return &BeelinePrefix{
		Prefix: prefix.Prefix(),
		JkId:   prefix.JkId(),
	}
}
