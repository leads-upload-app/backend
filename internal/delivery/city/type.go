package city

type CityListResponse struct {
	List []*City `json:"list"  binding:"required"`
}
type City struct {
	Id   int    `json:"id"  binding:"required"`
	Name string `json:"name"  binding:"required"`
}
