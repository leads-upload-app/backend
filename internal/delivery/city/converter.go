package city

import "amogo/internal/domain/city"

func ToResponseCity(city *city.City) *City {
	return &City{
		Id:   city.Id(),
		Name: city.Name(),
	}
}
