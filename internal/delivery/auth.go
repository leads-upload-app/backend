package delivery

import (
	jsonAuth "amogo/internal/delivery/auth"
	"context"
	"net/http"

	"github.com/gin-gonic/gin"
)

// SignIn
// @Summary Аутентификация в приложении.
// @Description Аутентификация в приложении.
// @Tags auth
// @Accept  json
// @Produce json
// @Param   contact 	body 		jsonAuth.SignInRequest 		    true  "Данные для аутентификации"
// @Success 200			{object}  	jsonAuth.SignInResponse
// @Failure 400 		{object}    ErrorResponse
// @Failure 401 		{object}    ErrorResponse
// @Router /auth/sign-in [post]
func (d *Delivery) SignIn(c *gin.Context) {
	credetinals := jsonAuth.SignInRequest{}
	if err := c.ShouldBindJSON(&credetinals); err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}

	tokens, err := d.services.Auth.SignIn(context.Background(), credetinals.Login, credetinals.Pass)
	if err != nil {
		SetError(c, http.StatusUnauthorized, "user not found")
		return
	}
	c.JSON(http.StatusOK, jsonAuth.SignInResponse{Token: tokens.Token, RefreshToken: tokens.Refresh})
}
