package delivery

import (
	"context"
	"net/http"

	"github.com/gin-gonic/gin"

	jsonPrefix "amogo/internal/delivery/beelinePrefixes"
)

// BeelinePrefixList
// @Summary Возвращает список соответсвия префиксов номеров (первые три цифры) с id жк.
// @Description Возвращает список соответсвия префиксов номеров (первые три цифры) с id жк.
// @Security ApiKeyAuth
// @Tags beeline
// @Accept  json
// @Produce json
// @Success 200			{object}  	jsonPrefix.PrefxListResponse
// @Failure 400 		{object}    ErrorResponse
// @Failure 401 		{object}    ErrorResponse
// @Router /beeline/prefix [get]
func (d *Delivery) BeelinePrefixList(c *gin.Context) {
	list, err := d.services.BeelinePrefixes.ReadBeelinePrefixList(context.Background())
	if err != nil {
		SetError(c, http.StatusInternalServerError, err.Error())
		return
	}
	resultList := []*jsonPrefix.BeelinePrefix{}
	for _, prefix := range list {
		resultList = append(resultList, jsonPrefix.ToResponsePrefix(prefix))
	}
	c.JSON(http.StatusOK, jsonPrefix.PrefxListResponse{List: resultList})
}

// BeelineUploadFile
// @Summary Парсит файл с таблицей соответствия и обновляет в базе.
// @Description Парсит файл с таблицей соответствия и обновляет в базе.
// @Security ApiKeyAuth
// @Tags beeline
// @Accept  multipart/form-data
// @Param file formData file true "File to upload"
// @Produce json
// @Success 201
// @Failure 400 		{object}    ErrorResponse
// @Failure 401 		{object}    ErrorResponse
// @Router /beeline/upload [put]
func (d *Delivery) BeelineUploadFile(c *gin.Context) {
	file, err := c.FormFile("file")
	if err != nil {
		SetError(c, http.StatusBadRequest, err.Error())
		return
	}
	reader, err := file.Open()
	if err != nil {
		SetError(c, http.StatusInternalServerError, err.Error())
		return
	}
	err = d.services.BeelinePrefixes.ProcessPrefixFile(context.Background(), reader)
	if err != nil {
		SetError(c, http.StatusInternalServerError, err.Error())
		return
	}
	c.Status(http.StatusCreated)
}
