package scheduler

import (
	"amogo/internal/service"
	"amogo/pkg/amocrmClient"
	"context"
	"time"

	"gitlab.com/kanya384/gotools/logger"
)

const (
	dailyUpdateInterval   = time.Hour * 24
	sleepIntervalOnError  = time.Minute * 20
	sendLeadsPauseOnEmpty = time.Second * 30
	leadsLimitKopilka     = 50
)

type scheduler struct {
	service             *service.Service
	client              *amocrmClient.Client
	logger              logger.Interface
	leadGoroutinesCount int
	leadsInPackageCount int
}

func NewScheduler(service *service.Service, client *amocrmClient.Client, logger logger.Interface, leadGoroutinesCount, leadsInPackageCount int) (*scheduler, error) {
	return &scheduler{
		leadGoroutinesCount: leadGoroutinesCount,
		leadsInPackageCount: leadsInPackageCount,
		client:              client,
		logger:              logger,
		service:             service,
	}, nil
}

func (s *scheduler) Run() {
	s.updateCitiesList(context.Background())
	s.updateAmoStatuses(context.Background())
	s.sendLeadsKopilka(context.Background())
	s.sendDmpLeadsToKopilka(context.Background())
	s.sendLeadsDirect(context.Background())
	s.refreshAmoToken(context.Background())
}

func (s *scheduler) refreshAmoToken(ctx context.Context) {
	go func(ctx context.Context, s *scheduler) {
		for {
			err := s.client.RefreshToken()
			if err != nil {
				s.logger.Error("error updating amoClient refresh token in scheduler: %s", err.Error())
				time.Sleep(sleepIntervalOnError)
				continue
			}
			time.Sleep(dailyUpdateInterval)
		}
	}(ctx, s)
}

func (s *scheduler) updateCitiesList(ctx context.Context) {
	go func(ctx context.Context, s *scheduler) {
		for {
			err := s.service.City.UpdateCities(ctx)
			if err != nil {
				s.logger.Error("error updating cities in scheduler: %s", err.Error())
				time.Sleep(sleepIntervalOnError)
				continue
			}
			time.Sleep(dailyUpdateInterval)
		}
	}(ctx, s)
}

func (s *scheduler) updateAmoStatuses(ctx context.Context) {
	go func(ctx context.Context, s *scheduler) {
		for {
			err := s.service.AmoStatus.UpdateStatuses(ctx)
			if err != nil {
				s.logger.Error("error updating amo statuses in scheduler: %s", err.Error())
				time.Sleep(sleepIntervalOnError)
				continue
			}
			time.Sleep(dailyUpdateInterval)
		}
	}(ctx, s)
}

func (s *scheduler) sendLeadsKopilka(ctx context.Context) {
	go func(ctx context.Context, s *scheduler) {
		for {
			count, err := s.service.Leads.SendLeadsKopilka(ctx, leadsLimitKopilka)
			if err != nil {
				s.logger.Error("error sending leads to kopilka in scheduler: %s", err.Error())
				continue
			}
			if count == 0 {
				time.Sleep(sendLeadsPauseOnEmpty)
			}
		}
	}(ctx, s)
}

func (s *scheduler) sendDmpLeadsToKopilka(ctx context.Context) {
	go func(ctx context.Context, s *scheduler) {
		for {
			count, err := s.service.DmpLeads.SendDmpLeadsToKopilka(ctx, leadsLimitKopilka)
			if err != nil {
				s.logger.Error("error sending dmp leads to kopilka in scheduler: %s", err.Error())
				continue
			}
			if count == 0 {
				time.Sleep(sendLeadsPauseOnEmpty)
			}
		}
	}(ctx, s)
}

func (s *scheduler) sendLeadsDirect(ctx context.Context) {
	go func(ctx context.Context, s *scheduler) {
		for {
			count, err := s.service.Leads.SendLeadsDirect(ctx, s.leadGoroutinesCount, s.leadsInPackageCount)
			if err != nil {
				s.logger.Error("error sending leads to amo in scheduler: %s", err.Error())
				time.Sleep(sleepIntervalOnError)
				continue
			}
			if count == 0 {
				time.Sleep(sendLeadsPauseOnEmpty)
			}
		}
	}(ctx, s)
}
