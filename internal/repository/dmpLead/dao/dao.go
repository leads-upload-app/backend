package dao

import (
	"time"

	"github.com/google/uuid"
)

type DmpLead struct {
	Id             uuid.UUID `db:"id"`
	Phone          string    `db:"phone"`
	Site           string    `db:"site"`
	SiteWithParams string    `db:"site_with_params"`
	Email          string    `db:"email"`
	Tag            string    `db:"tag"`
	JkId           int       `db:"jk_id"`
	JkName         string    `db:"jk_name"`
	CityId         string    `db:"city_id"`
	UtmMedium      string    `db:"utm_medium"`
	UtmTerm        string    `db:"utm_term"`
	UtmSource      string    `db:"utm_source"`
	UtmCampaign    string    `db:"utm_campaign"`
	UtmContent     string    `db:"utm_content"`
	Roistat        string    `db:"roistat"`
	Processed      bool      `db:"processed"`
	Yid            string    `db:"yid"`
	ResponseId     int       `db:"response_id"`
	CreatedAt      time.Time `db:"created_at"`
	ModifiedAt     time.Time `db:"modified_at"`
}

var ColumnsDmpLead = []string{
	"id",
	"phone",
	"site",
	"site_with_params",
	"email",
	"tag",
	"jk_id",
	"jk_name",
	"city_id",
	"utm_medium",
	"utm_term",
	"utm_source",
	"utm_campaign",
	"utm_content",
	"roistat",
	"processed",
	"yid",
	"response_id",
	"created_at",
	"modified_at",
}
