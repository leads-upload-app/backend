package repository

import (
	"amogo/internal/domain/dmpLead"
	"amogo/internal/repository/dmpLead/dao"
)

func (r *Repository) toDomainDmpLead(dao *dao.DmpLead) (*dmpLead.DmpLead, error) {
	return dmpLead.NewWithId(dao.Id, dao.Phone, dao.Site, dao.SiteWithParams, dao.Email, dao.Tag, dao.UtmMedium, dao.UtmTerm, dao.UtmSource, dao.UtmCampaign, dao.UtmContent, dao.Roistat, dao.Yid, dao.JkId, dao.JkName, dao.CityId, dao.Processed, dao.ResponseId, dao.CreatedAt, dao.ModifiedAt)
}
