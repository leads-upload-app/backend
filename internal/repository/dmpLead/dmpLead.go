package repository

import (
	"amogo/internal/domain/dmpLead"
	"amogo/internal/domain/dmpLead/filter"
	"amogo/internal/repository/dmpLead/dao"
	"context"
	"errors"
	"fmt"

	"github.com/Masterminds/squirrel"
	"github.com/google/uuid"
	"github.com/jackc/pgx/v5"
)

const (
	tableName = "dmp_lead"
)

var (
	ErrDuplicateKey = errors.New("ERROR: duplicate key value violates unique constraint \"dmp_lead_pkey\" (SQLSTATE 23505)")
	ErrNotFound     = errors.New("not found")
	ErrUpdate       = errors.New("error updating or no changes")
	ErrEmptyResult  = errors.New("no rows in result set")
)

func (r *Repository) CreateLead(ctx context.Context, dmpLead *dmpLead.DmpLead) (err error) {
	tx, err := r.Pool.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return
	}

	defer func(ctx context.Context, t pgx.Tx) error {
		if err != nil {
			if errRollback := t.Rollback(ctx); errRollback != nil {
				return fmt.Errorf("transaction rollback error: %s", err)
			}
			return err
		} else {
			if commitErr := tx.Commit(ctx); commitErr != nil {
				return fmt.Errorf("transaction commit error: %s", err)
			}
			return nil
		}
	}(ctx, tx)

	rawQuery := r.Builder.Insert(tableName).Columns(dao.ColumnsDmpLead...).Values(dmpLead.Id(), dmpLead.Phone(), dmpLead.Site(), dmpLead.SiteWithParams(), dmpLead.Email(), dmpLead.Tag(), dmpLead.JkId(), dmpLead.JkName(), dmpLead.CityId(), dmpLead.UtmMedium(), dmpLead.UtmTerm(), dmpLead.UtmSource(), dmpLead.UtmCampaign(), dmpLead.UtmContent(), dmpLead.Roistat(), dmpLead.Processed(), dmpLead.Yid(), dmpLead.ResponseId(), dmpLead.CreatedAt(), dmpLead.ModifiedAt())
	query, args, _ := rawQuery.ToSql()

	_, err = tx.Exec(ctx, query, args...)
	if err != nil {
		return
	}
	return
}

func (r *Repository) UpdateLead(ctx context.Context, Id uuid.UUID, updateFn func(lead *dmpLead.DmpLead) (*dmpLead.DmpLead, error)) (lead *dmpLead.DmpLead, err error) {
	tx, err := r.Pool.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return
	}

	defer func(ctx context.Context, t pgx.Tx) error {
		if err != nil {
			if errRollback := t.Rollback(ctx); errRollback != nil {
				return fmt.Errorf("transaction rollback error: %s", err)
			}
			return err
		} else {
			if commitErr := tx.Commit(ctx); commitErr != nil {
				return fmt.Errorf("transaction commit error: %s", err)
			}
			return nil
		}
	}(ctx, tx)

	upLead, err := r.oneLeadTx(ctx, tx, Id)
	if err != nil {
		return
	}

	leadNew, err := updateFn(upLead)
	if err != nil {
		return
	}

	rawQuery := r.Builder.Update(tableName).Set("processed", leadNew.Processed()).Set("response_id", leadNew.ResponseId()).Set("modified_at", leadNew.ModifiedAt()).Where("id = ?", leadNew.Id())
	query, args, _ := rawQuery.ToSql()

	res, err := tx.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	if res.RowsAffected() == 0 {
		return nil, ErrUpdate
	}

	return
}

func (r *Repository) GetUnsendedLeads(ctx context.Context, limit uint64) (leads []*dmpLead.DmpLead, err error) {
	tx, err := r.Pool.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return
	}

	defer func(ctx context.Context, t pgx.Tx) error {
		if err != nil {
			if errRollback := t.Rollback(ctx); errRollback != nil {
				return fmt.Errorf("transaction rollback error: %s", err)
			}
			return err
		} else {
			if commitErr := tx.Commit(ctx); commitErr != nil {
				return fmt.Errorf("transaction commit error: %s", err)
			}
			return nil
		}
	}(ctx, tx)

	rawQuery := r.Builder.Select(dao.ColumnsDmpLead...).From(tableName).Where("processed = ?", false).OrderBy("created_at").Limit(limit)

	query, args, _ := rawQuery.ToSql()

	row, err := r.Pool.Query(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	daoLead, err := pgx.CollectRows(row, pgx.RowToStructByPos[dao.DmpLead])
	if err != nil {
		return nil, err
	}

	leads = []*dmpLead.DmpLead{}

	for _, status := range daoLead {
		statusIns, err := r.toDomainDmpLead(&status)
		if err != nil {
			return nil, err
		}

		leads = append(leads, statusIns)
	}

	return
}

func (r *Repository) ReadLeadsFiltredList(ctx context.Context, filter filter.Filter, limit, offset int) (leads []*dmpLead.DmpLead, err error) {
	tx, err := r.Pool.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return
	}

	defer func(ctx context.Context, t pgx.Tx) error {
		if err != nil {
			if errRollback := t.Rollback(ctx); errRollback != nil {
				return fmt.Errorf("transaction rollback error: %s", err)
			}
			return err
		} else {
			if commitErr := tx.Commit(ctx); commitErr != nil {
				return fmt.Errorf("transaction commit error: %s", err)
			}
			return nil
		}
	}(ctx, tx)

	rawQuery := r.Builder.Select(dao.ColumnsDmpLead...).From(tableName).OrderBy("created_at")
	rawQuery = addFiltersToQuery(rawQuery, filter)
	query, args, _ := rawQuery.ToSql()

	row, err := r.Pool.Query(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	daoLead, err := pgx.CollectRows(row, pgx.RowToStructByPos[dao.DmpLead])
	if err != nil {
		return nil, err
	}

	leads = []*dmpLead.DmpLead{}

	for _, status := range daoLead {
		statusIns, err := r.toDomainDmpLead(&status)
		if err != nil {
			return nil, err
		}

		leads = append(leads, statusIns)
	}

	return
}

func (r *Repository) oneLeadTx(ctx context.Context, tx pgx.Tx, ID uuid.UUID) (response *dmpLead.DmpLead, err error) {
	rawQuery := r.Builder.Select(dao.ColumnsDmpLead...).From(tableName).Where("id = ?", ID)
	query, args, _ := rawQuery.ToSql()

	row, err := tx.Query(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	daoLead, err := pgx.CollectOneRow(row, pgx.RowToStructByPos[dao.DmpLead])
	if err != nil {
		return nil, err
	}

	return r.toDomainDmpLead(&daoLead)
}

func addFiltersToQuery(query squirrel.SelectBuilder, filter filter.Filter) squirrel.SelectBuilder {
	if !filter.DateStart.IsZero() {
		query = query.Where("created_at > ?", filter.DateStart)
	}

	if !filter.DateEnd.IsZero() {
		query = query.Where("created_at < ?", filter.DateEnd)
	}

	if filter.JkId != 0 {
		query = query.Where("jk_id = ?", filter.JkId)
	}

	return query
}
