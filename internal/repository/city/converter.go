package repository

import (
	"amogo/internal/domain/city"
	"amogo/internal/repository/city/dao"
)

func (r *Repository) toDomainCity(dao *dao.City) (*city.City, error) {
	return city.NewWithId(dao.Id, dao.Name, dao.CreatedAt, dao.ModifiedAt)
}
