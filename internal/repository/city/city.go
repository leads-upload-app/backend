package repository

import (
	"amogo/internal/domain/city"
	"amogo/internal/repository/city/dao"
	"context"
	"errors"
	"fmt"
	"time"

	"github.com/jackc/pgx/v5"
)

const (
	tableName = "city"
)

var (
	ErrDuplicateKey = errors.New("ERROR: duplicate key value violates unique constraint \"city_pkey\" (SQLSTATE 23505)")
	ErrNotFound     = errors.New("not found")
	ErrUpdate       = errors.New("error updating or no changes")
	ErrEmptyResult  = errors.New("no rows in result set")
)

func (r *Repository) UpdateCities(ctx context.Context, cityList []*city.City) (err error) {
	tx, err := r.Pool.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return
	}

	defer func(ctx context.Context, t pgx.Tx) error {
		if err != nil {
			if errRollback := t.Rollback(ctx); errRollback != nil {
				return fmt.Errorf("transaction rollback error: %s", err)
			}
			return err
		} else {
			if commitErr := tx.Commit(ctx); commitErr != nil {
				return fmt.Errorf("transaction commit error: %s", err)
			}
			return nil
		}
	}(ctx, tx)

	//clears table
	clearQuery := r.Builder.Delete(tableName).Where("created_at < ?", time.Now())
	deleteQuery, deleteArgs, _ := clearQuery.ToSql()
	_, err = tx.Exec(ctx, deleteQuery, deleteArgs...)
	if err != nil {
		return
	}

	//insert all new values
	rawQuery := r.Builder.Insert(tableName).Columns(dao.ColumnsCity...)
	for _, city := range cityList {
		//appends values into request
		rawQuery = rawQuery.Values(city.Id(), city.Name(), city.CreatedAt(), city.ModifiedAt())
	}

	query, args, _ := rawQuery.ToSql()
	_, err = tx.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return
}

func (r *Repository) ReadCityList(ctx context.Context) (cityList []*city.City, err error) {
	tx, err := r.Pool.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return
	}

	defer func(ctx context.Context, t pgx.Tx) error {
		if err != nil {
			if errRollback := t.Rollback(ctx); errRollback != nil {
				return fmt.Errorf("transaction rollback error: %s", err)
			}
			return err
		} else {
			if commitErr := tx.Commit(ctx); commitErr != nil {
				return fmt.Errorf("transaction commit error: %s", err)
			}
			return nil
		}
	}(ctx, tx)

	rawQuery := r.Builder.Select(dao.ColumnsCity...).From(tableName)
	query, args, _ := rawQuery.ToSql()

	row, err := r.Pool.Query(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	daoCity, err := pgx.CollectRows(row, pgx.RowToStructByPos[dao.City])
	if err != nil {
		return nil, err
	}

	cityList = []*city.City{}

	for _, city := range daoCity {
		cityIns, err := r.toDomainCity(&city)
		if err != nil {
			return nil, err
		}

		cityList = append(cityList, cityIns)
	}
	return
}
