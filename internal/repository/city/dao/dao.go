package dao

import (
	"time"
)

type City struct {
	Id         int       `db:"id"`
	Name       string    `db:"name"`
	CreatedAt  time.Time `db:"created_at"`
	ModifiedAt time.Time `db:"modified_at"`
}

var ColumnsCity = []string{
	"id",
	"name",
	"created_at",
	"modified_at",
}
