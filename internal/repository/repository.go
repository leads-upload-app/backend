package repository

import (
	amoStatus "amogo/internal/repository/amoStatus"
	auth "amogo/internal/repository/auth"
	beelinePrefixes "amogo/internal/repository/beelinePrefixes"
	city "amogo/internal/repository/city"
	dmpLead "amogo/internal/repository/dmpLead"
	lead "amogo/internal/repository/lead"

	"gitlab.com/kanya384/gotools/psql"
)

type Repository struct {
	AmoStatus       *amoStatus.Repository
	Auth            *auth.Repository
	BeelinePrefixes *beelinePrefixes.Repository
	City            *city.Repository
	Lead            *lead.Repository
	DmpLead         *dmpLead.Repository
}

func NewRepository(pg *psql.Postgres) (*Repository, error) {
	amoStatus, err := amoStatus.New(pg, amoStatus.Options{})
	if err != nil {
		return nil, err
	}
	auth, err := auth.New(pg, auth.Options{})
	if err != nil {
		return nil, err
	}

	beelinePrefixes, err := beelinePrefixes.New(pg, beelinePrefixes.Options{})
	if err != nil {
		return nil, err
	}

	city, err := city.New(pg, city.Options{})
	if err != nil {
		return nil, err
	}

	lead, err := lead.New(pg, lead.Options{})
	if err != nil {
		return nil, err
	}

	dmpLead, err := dmpLead.New(pg, dmpLead.Options{})
	if err != nil {
		return nil, err
	}

	return &Repository{
		amoStatus,
		auth,
		beelinePrefixes,
		city,
		lead,
		dmpLead,
	}, nil

}
