package repository

import (
	"amogo/internal/domain/lead"
	"amogo/internal/domain/lead/filter"
	"amogo/internal/repository/lead/dao"
	"context"
	"errors"
	"fmt"

	"github.com/Masterminds/squirrel"
	"github.com/google/uuid"
	"github.com/jackc/pgx/v5"
)

const (
	tableName = "lead"
)

var (
	ErrDuplicateKey = errors.New("ERROR: duplicate key value violates unique constraint \"lead_pkey\" (SQLSTATE 23505)")
	ErrNotFound     = errors.New("not found")
	ErrUpdate       = errors.New("error updating or no changes")
	ErrEmptyResult  = errors.New("no rows in result set")
)

func (r *Repository) CreateLead(ctx context.Context, lead *lead.Lead) (err error) {
	tx, err := r.Pool.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return
	}

	defer func(ctx context.Context, t pgx.Tx) error {
		if err != nil {
			if errRollback := t.Rollback(ctx); errRollback != nil {
				return fmt.Errorf("transaction rollback error: %s", err)
			}
			return err
		} else {
			if commitErr := tx.Commit(ctx); commitErr != nil {
				return fmt.Errorf("transaction commit error: %s", err)
			}
			return nil
		}
	}(ctx, tx)

	rawQuery := r.Builder.Insert(tableName).Columns(dao.ColumnsLead...).Values(lead.Id(), lead.Phone(), lead.JkId(), lead.JkName(), lead.JkDesc(), lead.Operator(), lead.CityId(), lead.PipelineId(), lead.StatusId(), lead.SendType(), lead.IsShort(), lead.Tags(), lead.UtmMedium(), lead.UtmTerm(), lead.UtmSource(), lead.UtmCampaign(), lead.IsJkFile(), lead.Processed(), lead.ResponseId(), lead.CreatedBy(), lead.CreatedAt(), lead.ModifiedAt())
	query, args, _ := rawQuery.ToSql()

	_, err = tx.Exec(ctx, query, args...)
	if err != nil {
		return
	}
	return
}

func (r *Repository) CreateLeadsList(ctx context.Context, leadsList []*lead.Lead) (err error) {
	tx, err := r.Pool.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return
	}

	defer func(ctx context.Context, t pgx.Tx) error {
		if err != nil {
			if errRollback := t.Rollback(ctx); errRollback != nil {
				return fmt.Errorf("transaction rollback error: %s", err)
			}
			return err
		} else {
			if commitErr := tx.Commit(ctx); commitErr != nil {
				return fmt.Errorf("transaction commit error: %s", err)
			}
			return nil
		}
	}(ctx, tx)

	// insert all new values
	rawQuery := fmt.Sprintf("INSERT INTO %s (id, phone, jk_id, jk_name,jk_desc,operator,city_id,pipeline_id,status_id,send_type,short,tags,utm_medium,utm_term,utm_source,utm_campaign,is_jk_file,processed,response_id,created_by,created_at,modified_at) VALUES ", tableName)
	for index, lead := range leadsList {
		if index > 0 {
			rawQuery += ","
		}

		rawQuery += fmt.Sprintf("('%s', '%s', %d, '%s', '%s', '%s', %d, %d, %d, '%s', %t, %s, '%s', '%s', '%s', '%s', %t, %t, %d, '%s', '%s', '%s' )", lead.Id(), lead.Phone(), lead.JkId(), lead.JkName(), lead.JkDesc(), lead.Operator(), lead.CityId(), lead.PipelineId(), lead.StatusId(), lead.SendType(), lead.IsShort(), arrayOfStringToInsertParam(lead.Tags()), lead.UtmMedium(), lead.UtmTerm(), lead.UtmSource(), lead.UtmCampaign(), lead.IsJkFile(), lead.Processed(), lead.ResponseId(), lead.CreatedBy(), lead.CreatedAt().Format("2006-01-02 15:04:05"), lead.ModifiedAt().Format("2006-01-02 15:04:05"))
	}
	rawQuery += ";"

	_, err = tx.Exec(ctx, rawQuery)
	if err != nil {
		return
	}

	return
}

func arrayOfStringToInsertParam(input []string) string {
	if len(input) == 0 {
		return "ARRAY ['']"
	}
	result := "ARRAY ["
	for _, val := range input {
		result += fmt.Sprintf("'%s'", val)
	}
	result += "]"
	return result
}

func (r *Repository) UpdateLead(ctx context.Context, Id uuid.UUID, updateFn func(lead *lead.Lead) (*lead.Lead, error)) (lead *lead.Lead, err error) {
	tx, err := r.Pool.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return
	}

	defer func(ctx context.Context, t pgx.Tx) error {
		if err != nil {
			if errRollback := t.Rollback(ctx); errRollback != nil {
				return fmt.Errorf("transaction rollback error: %s", err)
			}
			return err
		} else {
			if commitErr := tx.Commit(ctx); commitErr != nil {
				return fmt.Errorf("transaction commit error: %s", err)
			}
			return nil
		}
	}(ctx, tx)

	upLead, err := r.oneLeadTx(ctx, tx, Id)
	if err != nil {
		return
	}

	leadNew, err := updateFn(upLead)
	if err != nil {
		return
	}

	rawQuery := r.Builder.Update(tableName).Set("phone", leadNew.Phone()).Set("jk_id", leadNew.JkId()).Set("jk_name", leadNew.JkName()).Set("jk_desc", leadNew.JkDesc()).Set("operator", leadNew.Operator()).Set("city_id", leadNew.CityId()).Set("status_id", leadNew.StatusId()).Set("send_type", leadNew.SendType()).Set("short", leadNew.IsShort()).Set("tags", leadNew.Tags()).Set("utm_medium", leadNew.UtmMedium()).Set("utm_term", leadNew.UtmTerm()).Set("utm_source", leadNew.UtmSource()).Set("utm_campaign", leadNew.UtmCampaign()).Set("is_jk_file", leadNew.IsJkFile()).Set("created_by", leadNew.CreatedBy()).Set("processed", leadNew.Processed()).Set("response_id", leadNew.ResponseId()).Set("modified_at", leadNew.ModifiedAt()).Where("id = ?", leadNew.Id())
	query, args, _ := rawQuery.ToSql()

	res, err := tx.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	if res.RowsAffected() == 0 {
		return nil, ErrUpdate
	}

	return
}

func (r *Repository) DeleteLead(ctx context.Context, id uuid.UUID) (err error) {
	tx, err := r.Pool.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return
	}

	defer func(ctx context.Context, t pgx.Tx) error {
		if err != nil {
			if errRollback := t.Rollback(ctx); errRollback != nil {
				return fmt.Errorf("transaction rollback error: %s", err)
			}
			return err
		} else {
			if commitErr := tx.Commit(ctx); commitErr != nil {
				return fmt.Errorf("transaction commit error: %s", err)
			}
			return nil
		}
	}(ctx, tx)

	rawQuery := r.Builder.Delete(tableName).Where("id = ?", id)
	query, args, _ := rawQuery.ToSql()

	res, err := tx.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	if res.RowsAffected() == 0 {
		return ErrNotFound
	}

	return
}

func (r *Repository) GetUnsendedLeads(ctx context.Context, leadType lead.SendTypes, limit uint64) (leads []*lead.Lead, err error) {
	tx, err := r.Pool.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return
	}

	defer func(ctx context.Context, t pgx.Tx) error {
		if err != nil {
			if errRollback := t.Rollback(ctx); errRollback != nil {
				return fmt.Errorf("transaction rollback error: %s", err)
			}
			return err
		} else {
			if commitErr := tx.Commit(ctx); commitErr != nil {
				return fmt.Errorf("transaction commit error: %s", err)
			}
			return nil
		}
	}(ctx, tx)

	rawQuery := r.Builder.Select(dao.ColumnsLead...).From(tableName).Where("send_type = ? and processed = ?", leadType, false).OrderBy("created_at").Limit(limit)

	query, args, _ := rawQuery.ToSql()

	row, err := r.Pool.Query(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	daoLead, err := pgx.CollectRows(row, pgx.RowToStructByPos[dao.Lead])
	if err != nil {
		return nil, err
	}

	leads = []*lead.Lead{}

	for _, status := range daoLead {
		statusIns, err := r.toDomainLead(&status)
		if err != nil {
			return nil, err
		}

		leads = append(leads, statusIns)
	}

	return
}

func (r *Repository) GetUnsendedLeadsCount(ctx context.Context, sendType lead.SendTypes) (count int, err error) {
	tx, err := r.Pool.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return
	}

	defer func(ctx context.Context, t pgx.Tx) error {
		if err != nil {
			if errRollback := t.Rollback(ctx); errRollback != nil {
				return fmt.Errorf("transaction rollback error: %s", err)
			}
			return err
		} else {
			if commitErr := tx.Commit(ctx); commitErr != nil {
				return fmt.Errorf("transaction commit error: %s", err)
			}
			return nil
		}
	}(ctx, tx)

	rawQuery := r.Builder.Select("COUNT (*)").From(tableName).Where("send_type = ? and processed = ?", sendType, false)
	query, args, _ := rawQuery.ToSql()

	row, err := r.Pool.Query(ctx, query, args...)
	if err != nil {
		return
	}

	result, err := pgx.CollectOneRow(row, pgx.RowToStructByPos[dao.Count])
	if err != nil {
		return
	}

	count = result.Length

	return
}

func (r *Repository) ReadLeadsFiltredList(ctx context.Context, filter filter.Filter, limit, offset int) (leads []*lead.Lead, err error) {
	tx, err := r.Pool.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return
	}

	defer func(ctx context.Context, t pgx.Tx) error {
		if err != nil {
			if errRollback := t.Rollback(ctx); errRollback != nil {
				return fmt.Errorf("transaction rollback error: %s", err)
			}
			return err
		} else {
			if commitErr := tx.Commit(ctx); commitErr != nil {
				return fmt.Errorf("transaction commit error: %s", err)
			}
			return nil
		}
	}(ctx, tx)

	rawQuery := r.Builder.Select(dao.ColumnsLead...).From(tableName).OrderBy("created_at")
	rawQuery = addFiltersToQuery(rawQuery, filter)
	query, args, _ := rawQuery.ToSql()

	row, err := r.Pool.Query(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	daoLead, err := pgx.CollectRows(row, pgx.RowToStructByPos[dao.Lead])
	if err != nil {
		return nil, err
	}

	leads = []*lead.Lead{}

	for _, status := range daoLead {
		statusIns, err := r.toDomainLead(&status)
		if err != nil {
			return nil, err
		}

		leads = append(leads, statusIns)
	}

	return
}

func (r *Repository) oneLeadTx(ctx context.Context, tx pgx.Tx, ID uuid.UUID) (response *lead.Lead, err error) {
	rawQuery := r.Builder.Select(dao.ColumnsLead...).From(tableName).Where("id = ?", ID)
	query, args, _ := rawQuery.ToSql()

	row, err := tx.Query(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	daoLead, err := pgx.CollectOneRow(row, pgx.RowToStructByPos[dao.Lead])
	if err != nil {
		return nil, err
	}

	return r.toDomainLead(&daoLead)
}

func addFiltersToQuery(query squirrel.SelectBuilder, filter filter.Filter) squirrel.SelectBuilder {
	/*if filter.CreatedBy != uuid.Nil {
		query = query.Where("created_by = ?", filter.CreatedBy)
	}*/

	if !filter.DateStart.IsZero() {
		query = query.Where("created_at > ?", filter.DateStart)
	}

	if !filter.DateEnd.IsZero() {
		query = query.Where("created_at < ?", filter.DateEnd)
	}

	if filter.SendType != "" {
		query = query.Where("send_type = ?", filter.SendType)
	}

	if filter.Tag != "" {
		query = query.Where("? = ANY (tags)", filter.Tag)
	}

	if filter.UtmMedium != "" {
		query = query.Where("utm_medium = ?", filter.UtmMedium)
	}

	if filter.UtmTerm != "" {
		query = query.Where("utm_medium = ?", filter.UtmTerm)
	}

	if filter.UtmCampaign != "" {
		query = query.Where("utm_campaign = ?", filter.UtmCampaign)
	}

	if filter.UtmSource != "" {
		query = query.Where("utm_source = ?", filter.UtmSource)
	}

	if filter.UtmSource != "" {
		query = query.Where("utm_source = ?", filter.UtmSource)
	}

	return query
}
