package repository

import (
	"amogo/internal/domain/lead"
	"amogo/internal/repository/lead/dao"
)

func (r *Repository) toDomainLead(dao *dao.Lead) (*lead.Lead, error) {
	return lead.NewWithID(dao.Id, dao.Phone, dao.JkId, dao.JkName, dao.JkDesc, lead.OperatorTypes(dao.Operator), dao.CityId, dao.PipelineId, dao.StatusId, lead.SendTypes(dao.SendType), dao.Short, dao.Tags, dao.UtmMedium, dao.UtmTerm, dao.UtmSource, dao.UtmCampaign, dao.IsJkFile, dao.Processed, dao.ResponseId, dao.CreatedBy, dao.CreatedAt, dao.ModifiedAt)
}
