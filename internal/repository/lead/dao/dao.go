package dao

import (
	"time"

	"github.com/google/uuid"
)

type Lead struct {
	Id          uuid.UUID `db:"id"`
	Phone       string    `db:"phone"`
	JkId        int       `db:"jk_id"`
	JkName      string    `db:"jk_name"`
	JkDesc      string    `db:"jk_desc"`
	Operator    string    `db:"operator"`
	CityId      int       `db:"city_id"`
	PipelineId  int       `db:"pipeline_id"`
	StatusId    int       `db:"status_id"`
	SendType    string    `db:"send_type"`
	Short       bool      `db:"short"`
	Tags        []string  `db:"tags"`
	UtmMedium   string    `db:"utm_medium"`
	UtmTerm     string    `db:"utm_term"`
	UtmSource   string    `db:"utm_source"`
	UtmCampaign string    `db:"utm_campaign"`
	IsJkFile    bool      `db:"is_jk_file"`
	Processed   bool      `db:"processed"`
	ResponseId  int       `db:"response_id"`
	CreatedBy   uuid.UUID `db:"created_by"`
	CreatedAt   time.Time `db:"created_at"`
	ModifiedAt  time.Time `db:"modified_at"`
}

var ColumnsLead = []string{
	"id",
	"phone",
	"jk_id",
	"jk_name",
	"jk_desc",
	"operator",
	"city_id",
	"pipeline_id",
	"status_id",
	"send_type",
	"short",
	"tags",
	"utm_medium",
	"utm_term",
	"utm_source",
	"utm_campaign",
	"is_jk_file",
	"processed",
	"response_id",
	"created_by",
	"created_at",
	"modified_at",
}

type Count struct {
	Length int `db:"count"`
}
