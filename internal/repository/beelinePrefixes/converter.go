package repository

import (
	beelinePrefix "amogo/internal/domain/beelinePrefixes"
	"amogo/internal/repository/beelinePrefixes/dao"
)

func (r *Repository) toDomainBeelinePrefx(dao *dao.BeelinePrefix) (*beelinePrefix.BeelinePrefix, error) {
	return beelinePrefix.NewWithId(dao.Id, dao.Prefix, dao.JkId, dao.CreatedAt, dao.ModifiedAt)
}
