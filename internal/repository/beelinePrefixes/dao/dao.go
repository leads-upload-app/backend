package dao

import (
	"time"

	"github.com/google/uuid"
)

type BeelinePrefix struct {
	Id         uuid.UUID `db:"id"`
	Prefix     int       `db:"prefix"`
	JkId       int       `db:"jk_id"`
	CreatedAt  time.Time `db:"created_at"`
	ModifiedAt time.Time `db:"updated_at"`
}

var ColumnsBeelinePrefix = []string{
	"id",
	"prefix",
	"jk_id",
	"created_at",
	"updated_at",
}
