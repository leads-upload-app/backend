package repository

import (
	beelinePrefix "amogo/internal/domain/beelinePrefixes"
	"amogo/internal/repository/beelinePrefixes/dao"

	"context"
	"errors"
	"fmt"

	"github.com/jackc/pgx/v5"
)

const (
	tableName = "beeline_prefixes"
)

var (
	ErrDuplicateKey = errors.New("ERROR: duplicate key value violates unique constraint \"beeline_prefixes_pkey\" (SQLSTATE 23505)")
	ErrNotFound     = errors.New("not found")
	ErrUpdate       = errors.New("error updating or no changes")
	ErrEmptyResult  = errors.New("no rows in result set")
)

func (r *Repository) ReplacePrefixes(ctx context.Context, prefixList []*beelinePrefix.BeelinePrefix) (err error) {
	tx, err := r.Pool.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return
	}

	defer func(ctx context.Context, t pgx.Tx) error {
		if err != nil {
			if errRollback := t.Rollback(ctx); errRollback != nil {
				return fmt.Errorf("transaction rollback error: %s", err)
			}
			return err
		} else {
			if commitErr := tx.Commit(ctx); commitErr != nil {
				return fmt.Errorf("transaction commit error: %s", err)
			}
			return nil
		}
	}(ctx, tx)

	//clears table
	clearQuery := r.Builder.Delete(tableName)
	deleteQuery, deleteArgs, _ := clearQuery.ToSql()
	_, err = tx.Exec(ctx, deleteQuery, deleteArgs...)
	if err != nil {
		return
	}

	//insert all new values
	rawQuery := r.Builder.Insert(tableName).Columns(dao.ColumnsBeelinePrefix...)
	for _, prefix := range prefixList {
		//appends values into request
		rawQuery = rawQuery.Values(prefix.Id(), prefix.Prefix(), prefix.JkId(), prefix.CreatedAt(), prefix.ModifiedAt())
	}

	query, args, _ := rawQuery.ToSql()
	_, err = tx.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return
}

func (r *Repository) ReadBeelinePrefixList(ctx context.Context) (prefixes []*beelinePrefix.BeelinePrefix, err error) {
	tx, err := r.Pool.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return
	}

	defer func(ctx context.Context, t pgx.Tx) error {
		if err != nil {
			if errRollback := t.Rollback(ctx); errRollback != nil {
				return fmt.Errorf("transaction rollback error: %s", err)
			}
			return err
		} else {
			if commitErr := tx.Commit(ctx); commitErr != nil {
				return fmt.Errorf("transaction commit error: %s", err)
			}
			return nil
		}
	}(ctx, tx)

	rawQuery := r.Builder.Select(dao.ColumnsBeelinePrefix...).From(tableName)
	query, args, _ := rawQuery.ToSql()

	row, err := r.Pool.Query(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	daoAmoPrefix, err := pgx.CollectRows(row, pgx.RowToStructByPos[dao.BeelinePrefix])
	if err != nil {
		return nil, err
	}

	prefixes = []*beelinePrefix.BeelinePrefix{}

	for _, prefix := range daoAmoPrefix {
		prefixIns, err := r.toDomainBeelinePrefx(&prefix)
		if err != nil {
			return nil, err
		}

		prefixes = append(prefixes, prefixIns)
	}

	return
}
