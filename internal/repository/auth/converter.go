package repository

import (
	"amogo/internal/domain/user"
	"amogo/internal/repository/auth/dao"
)

func (r *Repository) toDomainUser(dao *dao.User) (*user.User, error) {
	return user.NewWithID(dao.Id, dao.Name, dao.Login, "", dao.CreatedAt, dao.ModifiedAt) // pass is empty for security reasons
}
