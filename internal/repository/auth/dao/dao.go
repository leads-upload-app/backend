package dao

import (
	"time"

	"github.com/google/uuid"
)

type User struct {
	Id         uuid.UUID `db:"id"`
	Name       string    `db:"name"`
	Login      string    `db:"login"`
	Pass       string    `db:"pass"`
	CreatedAt  time.Time `db:"created_at"`
	ModifiedAt time.Time `db:"modified_at"`
}

var ColumnsUser = []string{
	"id",
	"name",
	"login",
	"pass",
	"created_at",
	"modified_at",
}
