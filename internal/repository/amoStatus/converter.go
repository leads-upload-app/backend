package repository

import (
	"amogo/internal/domain/amoStatus"
	"amogo/internal/repository/amoStatus/dao"
)

func (r *Repository) toDomainAmoStatus(dao *dao.AmoStatus) (*amoStatus.AmoStatus, error) {
	return amoStatus.NewWithId(dao.Id, dao.PipelineName, dao.PipelineId, dao.StatusName, dao.StatusId, dao.CreatedAt, dao.ModifiedAt)
}
