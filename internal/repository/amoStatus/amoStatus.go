package repository

import (
	"amogo/internal/domain/amoStatus"
	"amogo/internal/repository/amoStatus/dao"
	"context"
	"errors"
	"fmt"

	"github.com/jackc/pgx/v5"
)

const (
	tableName = "amo_statuses"
)

var (
	ErrDuplicateKey = errors.New("ERROR: duplicate key value violates unique constraint \"amoStatus_pkey\" (SQLSTATE 23505)")
	ErrNotFound     = errors.New("not found")
	ErrUpdate       = errors.New("error updating or no changes")
	ErrEmptyResult  = errors.New("no rows in result set")
)

func (r *Repository) UpdateAmoStatuses(ctx context.Context, amoStatusList []*amoStatus.AmoStatus) (err error) {
	tx, err := r.Pool.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return
	}

	defer func(ctx context.Context, t pgx.Tx) error {
		if err != nil {
			if errRollback := t.Rollback(ctx); errRollback != nil {
				return fmt.Errorf("transaction rollback error: %s", err)
			}
			return err
		} else {
			if commitErr := tx.Commit(ctx); commitErr != nil {
				return fmt.Errorf("transaction commit error: %s", err)
			}
			return nil
		}
	}(ctx, tx)

	//clears table
	clearQuery := r.Builder.Delete(tableName)
	deleteQuery, deleteArgs, _ := clearQuery.ToSql()
	_, err = tx.Exec(ctx, deleteQuery, deleteArgs...)
	if err != nil {
		return
	}

	//insert all new values
	rawQuery := r.Builder.Insert(tableName).Columns(dao.ColumnsAmoStatus...)
	for _, amoStatus := range amoStatusList {
		//appends values into request
		rawQuery = rawQuery.Values(amoStatus.Id(), amoStatus.PipelineName(), amoStatus.PipelineId(), amoStatus.StatusName(), amoStatus.StatusId(), amoStatus.CreatedAt(), amoStatus.ModifiedAt())
	}

	query, args, _ := rawQuery.ToSql()
	_, err = tx.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return
}

func (r *Repository) ReadAmoStatusList(ctx context.Context) (statuses []*amoStatus.AmoStatus, err error) {
	tx, err := r.Pool.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return
	}

	defer func(ctx context.Context, t pgx.Tx) error {
		if err != nil {
			if errRollback := t.Rollback(ctx); errRollback != nil {
				return fmt.Errorf("transaction rollback error: %s", err)
			}
			return err
		} else {
			if commitErr := tx.Commit(ctx); commitErr != nil {
				return fmt.Errorf("transaction commit error: %s", err)
			}
			return nil
		}
	}(ctx, tx)

	rawQuery := r.Builder.Select(dao.ColumnsAmoStatus...).From(tableName)
	query, args, _ := rawQuery.ToSql()

	row, err := r.Pool.Query(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	daoAmoStatus, err := pgx.CollectRows(row, pgx.RowToStructByPos[dao.AmoStatus])
	if err != nil {
		return nil, err
	}

	statuses = []*amoStatus.AmoStatus{}

	for _, status := range daoAmoStatus {
		statusIns, err := r.toDomainAmoStatus(&status)
		if err != nil {
			return nil, err
		}

		statuses = append(statuses, statusIns)
	}

	return
}
