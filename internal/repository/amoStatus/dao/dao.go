package dao

import (
	"time"

	"github.com/google/uuid"
)

type AmoStatus struct {
	Id           uuid.UUID `db:"id"`
	PipelineName string    `db:"pipeline_name"`
	PipelineId   int       `db:"pipeline_id"`
	StatusName   string    `db:"status_name"`
	StatusId     int       `db:"status_id"`
	CreatedAt    time.Time `db:"created_at"`
	ModifiedAt   time.Time `db:"modified_at"`
}

var ColumnsAmoStatus = []string{
	"id",
	"pipeline_name",
	"pipeline_id",
	"status_name",
	"status_id",
	"created_at",
	"modified_at",
}
