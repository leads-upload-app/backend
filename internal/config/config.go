package config

import (
	"fmt"

	"github.com/vrischmann/envconfig"
)

type Config struct {
	App struct {
		Port        int
		ServiceName string
	}

	Amo struct {
		ClientId     string
		ClientSecret string
		RedirectUri  string
	}

	PassSalt string

	Log struct {
		Level string
		Path  string
	}

	Portal struct {
		Url string
		Key string
	}

	PG struct {
		User    string
		Pass    string
		Port    string
		Host    string
		PoolMax int
		DbName  string
		Timeout int
	}

	DirectSend struct {
		GoroutinesCount     int
		LeadsInPackageCount int
	}

	Token struct {
		Secret string
	}
}

func InitConfig(prefix string) (*Config, error) {
	conf := &Config{}
	if err := envconfig.InitWithPrefix(conf, prefix); err != nil {
		return nil, fmt.Errorf("init config error: %w", err)
	}

	return conf, nil
}
