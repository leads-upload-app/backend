package service

import (
	"amogo/internal/domain/city"
	"context"
)

type Service interface {
	UpdateCities(ctx context.Context) (err error)
	ReadCityList(ctx context.Context) (statuses []*city.City, err error)
}
