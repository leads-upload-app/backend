package service

import (
	"amogo/internal/domain/city"
	"amogo/pkg/gnApi"
	"strconv"
)

func (r *service) convertToDomainCity(apiCity *gnApi.City) (*city.City, error) {
	id, err := strconv.Atoi(apiCity.ID)
	if err != nil {
		return nil, err
	}
	return city.New(id, apiCity.Name)
}
