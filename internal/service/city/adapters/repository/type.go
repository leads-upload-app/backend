package repository

import (
	"amogo/internal/domain/city"
	"context"
)

type Repository interface {
	UpdateCities(ctx context.Context, cityList []*city.City) (err error)
	ReadCityList(ctx context.Context) (statuses []*city.City, err error)
}
