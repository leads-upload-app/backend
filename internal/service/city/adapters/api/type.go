package api

import (
	"amogo/pkg/gnApi"
	"context"
)

type Api interface {
	GetCityList(ctx context.Context) (cityList []*gnApi.City, err error)
}
