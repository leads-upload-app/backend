package service

import (
	"amogo/internal/repository"
	"amogo/internal/service/city/adapters/api"

	"gitlab.com/kanya384/gotools/logger"
)

type service struct {
	repository *repository.Repository
	logger     logger.Interface
	options    Options
	api        api.Api
}

type Options struct {
}

func (s *service) SetOptions(options Options) {
	if s.options != options {
		s.options = options
	}
}

func New(repository *repository.Repository, logger logger.Interface, api api.Api, options Options) (*service, error) {
	service := &service{
		repository: repository,
		logger:     logger,
		api:        api,
	}

	service.SetOptions(options)
	return service, nil
}
