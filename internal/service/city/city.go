package service

import (
	"amogo/internal/domain/city"
	"context"
	"time"
)

func (s *service) UpdateCities(ctx context.Context) (err error) {
	ctx, cancel := context.WithTimeout(ctx, time.Second*10)
	defer cancel()

	cityListApi, err := s.api.GetCityList(ctx)
	if err != nil {
		return
	}

	cityList := make([]*city.City, len(cityListApi))
	for index, cityApi := range cityListApi {
		city, err := s.convertToDomainCity(cityApi)
		if err != nil {
			return err
		}
		cityList[index] = city
	}

	err = s.repository.City.UpdateCities(ctx, cityList)
	return
}

func (s *service) ReadCityList(ctx context.Context) (cityList []*city.City, err error) {
	cityList, err = s.repository.City.ReadCityList(ctx)
	if err != nil {
		s.logger.Error("ReadCityList error: %s", err.Error())
	}
	return
}
