package service

import (
	"amogo/internal/repository"

	"gitlab.com/kanya384/gotools/logger"
)

type service struct {
	repository *repository.Repository
	logger     logger.Interface
	options    Options
}

type Options struct {
}

func (uc *service) SetOptions(options Options) {
	if uc.options != options {
		uc.options = options
	}
}

func New(repository *repository.Repository, logger logger.Interface, options Options) (*service, error) {
	service := &service{
		repository: repository,
		logger:     logger,
	}

	service.SetOptions(options)
	return service, nil
}
