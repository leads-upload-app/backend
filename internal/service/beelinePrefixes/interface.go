package service

import (
	beelinePrefix "amogo/internal/domain/beelinePrefixes"
	"context"
	"mime/multipart"
)

type Service interface {
	ProcessPrefixFile(ctx context.Context, reader multipart.File) (err error)
	ReadBeelinePrefixList(ctx context.Context) (statuses []*beelinePrefix.BeelinePrefix, err error)
}
