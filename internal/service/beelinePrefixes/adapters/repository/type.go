package repository

import (
	beelinePrefix "amogo/internal/domain/beelinePrefixes"
	"context"
)

type Repository interface {
	ReplacePrefixes(ctx context.Context, prefix []*beelinePrefix.BeelinePrefix) (err error)
	ReadBeelinePrefixList(ctx context.Context) (statuses []*beelinePrefix.BeelinePrefix, err error)
}
