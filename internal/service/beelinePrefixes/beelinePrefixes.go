package service

import (
	beelinePrefix "amogo/internal/domain/beelinePrefixes"
	"amogo/pkg/excel"
	"context"
	"mime/multipart"
	"strconv"
	"strings"
)

func (s *service) ReplacePrefixes(ctx context.Context, prefix []*beelinePrefix.BeelinePrefix) (err error) {
	err = s.repository.BeelinePrefixes.ReplacePrefixes(ctx, prefix)
	if err != nil {
		s.logger.Error("ReplacePrefixes error: %s", err.Error())
	}
	return
}

func (s *service) ProcessPrefixFile(ctx context.Context, reader multipart.File) (err error) {

	rows, err := excel.NewExcelService().ParseFile(reader)
	if err != nil {
		s.logger.Error("ProcessPrefixFile error: %s", err.Error())
		return err
	}

	prefixList := []*beelinePrefix.BeelinePrefix{}
	for index, row := range rows {
		pref, err := strconv.Atoi(row[0])
		if err != nil {
			s.logger.Error("ProcessPrefixFile error converting to int prefix line %d error: %s", index, err.Error())
			return err
		}

		jkId, err := strconv.Atoi(strings.Trim(row[1], "\t\n"))

		if err != nil {
			s.logger.Error("ProcessPrefixFile error converting to int jkId line %d error: %s", index, err.Error())
			return err
		}
		prefix, _ := beelinePrefix.New(pref, jkId)
		prefixList = append(prefixList, prefix)
	}

	err = s.repository.BeelinePrefixes.ReplacePrefixes(ctx, prefixList)
	if err != nil {
		s.logger.Error("ProcessPrefixFile store to repo error: %s", err.Error())
	}
	return
}

func (s *service) ReadBeelinePrefixList(ctx context.Context) (prefixes []*beelinePrefix.BeelinePrefix, err error) {
	prefixes, err = s.repository.BeelinePrefixes.ReadBeelinePrefixList(ctx)
	if err != nil {
		s.logger.Error("ReadBeelinePrefixList error: %s", err.Error())
	}
	return
}
