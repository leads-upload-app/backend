package service

import (
	"amogo/internal/domain/user"
	"context"

	"github.com/google/uuid"
)

type Service interface {
	SignIn(ctx context.Context, login, pass string) (res SignInResult, err error)
	SignUp(ctx context.Context, user *user.User) (err error)
	RefreshToken(ctx context.Context, userID uuid.UUID, refreshToken string) (result RefreshResult, err error)
}
