package service

import (
	"amogo/internal/domain/amoStatus"
	"amogo/pkg/amocrmClient"
)

func (s *service) toDomainStatus(pipelineName string, pipelineId int, status *amocrmClient.Status) (*amoStatus.AmoStatus, error) {
	return amoStatus.New(pipelineName, pipelineId, status.Name, status.Id)
}
