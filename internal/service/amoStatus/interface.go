package service

import (
	"amogo/internal/domain/amoStatus"
	"context"
)

type Service interface {
	UpdateStatuses(ctx context.Context) (err error)
	ReadAmoStatusList(ctx context.Context) (statuses []*amoStatus.AmoStatus, err error)
}
