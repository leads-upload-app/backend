package repository

import (
	"amogo/internal/domain/amoStatus"
	"context"
)

type Repository interface {
	UpdateAmoStatuses(ctx context.Context, amoStatusList []*amoStatus.AmoStatus) (err error)
	ReadAmoStatusList(ctx context.Context) (statuses []*amoStatus.AmoStatus, err error)
}
