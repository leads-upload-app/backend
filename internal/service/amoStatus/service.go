package service

import (
	"amogo/internal/repository"

	"amogo/pkg/amocrmClient"

	"gitlab.com/kanya384/gotools/logger"
)

type service struct {
	repository *repository.Repository
	logger     logger.Interface
	amoClient  *amocrmClient.Client
	options    Options
}

type Options struct {
}

func (uc *service) SetOptions(options Options) {
	if uc.options != options {
		uc.options = options
	}
}

func New(repository *repository.Repository, amoClient *amocrmClient.Client, logger logger.Interface, options Options) *service {
	var uc = &service{
		repository: repository,
		logger:     logger,
		amoClient:  amoClient,
	}
	uc.SetOptions(options)
	return uc
}
