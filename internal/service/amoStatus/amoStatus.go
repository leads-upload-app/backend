package service

import (
	"amogo/internal/domain/amoStatus"
	"context"
)

func (u *service) UpdateStatuses(ctx context.Context) (err error) {
	list, err := u.amoClient.GetPipelinesList()
	if err != nil {
		u.logger.Error("UpdateAmoStatuses request pipelines list error: %s", err.Error())
		return
	}
	statuses := []*amoStatus.AmoStatus{}
	for _, pipeline := range list {
		for _, apiStatus := range pipeline.Embeeded.Statuses {
			status, err := u.toDomainStatus(pipeline.Name, pipeline.Id, &apiStatus)
			if err != nil {
				u.logger.Error("UpdateAmoStatuses convert status to domain struct error: %s", err.Error())
				return err
			}
			statuses = append(statuses, status)
		}
	}

	err = u.repository.AmoStatus.UpdateAmoStatuses(context.Background(), statuses)
	if err != nil {
		u.logger.Error("UpdateAmoStatuses store statuses error: %s", err.Error())
	}
	//u.repository.AmoStatus.
	return
}

func (u *service) ReadAmoStatusList(ctx context.Context) (statuses []*amoStatus.AmoStatus, err error) {
	statuses, err = u.repository.AmoStatus.ReadAmoStatusList(ctx)
	if err != nil {
		u.logger.Error("ReadAmoStatusList error: %s", err.Error())
	}
	return
}
