package service

import "github.com/google/uuid"

type CreateParams struct {
	Phones      []string
	JkId        int
	JkName      string
	JkDesc      string
	Operator    string
	CityId      int
	PipelineId  int
	StatusId    int
	SendType    string
	Short       bool
	Tags        []string
	UtmMedium   string
	UtmTerm     string
	UtmSource   string
	UtmCampaign string
	IsJkFile    bool
	CreatedBy   uuid.UUID
}
