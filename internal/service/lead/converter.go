package service

import (
	"amogo/internal/domain/city"
	"amogo/internal/domain/lead"
	amoContact "amogo/pkg/amocrmClient/contact"
	amoContactCustomField "amogo/pkg/amocrmClient/contact/customField"
	"amogo/pkg/amocrmClient/customField"
	amoEmbeeded "amogo/pkg/amocrmClient/embeeded"
	amoLead "amogo/pkg/amocrmClient/lead"
	amoTag "amogo/pkg/amocrmClient/tag"
	"strconv"
)

func (ls *leadsService) toAmoApiLead(leadIndex int, lead *lead.Lead, cityList []*city.City) *amoLead.Lead {
	contact := &amoContact.Contact{}
	contactPhone := amoContactCustomField.CustomField{
		FieldCode: "PHONE",
		Values: []amoContactCustomField.Enum{
			{
				EnumCode: "WORK",
				Value:    lead.Phone(),
			},
		},
	}
	contact.CustomFieldValues = append(contact.CustomFieldValues, contactPhone)

	tags := []*amoTag.Tag{}
	for _, tagIn := range lead.Tags() {
		if lead.IsJkFile() && tagIn != "" {
			tagIn += "_jk"
		}
		tag := amoTag.Tag{
			Name: tagIn,
		}
		tags = append(tags, &tag)
	}

	if lead.Operator() != "" {
		operatorTag := ""
		switch lead.Operator() {
		case "beeline":
			operatorTag = "Lead_Beeline"
		case "tele2":
			operatorTag = "Lead_Tele2"
		case "mts":
			operatorTag = "Lead_MTS"
		case "megafon":
			operatorTag = "Lead_Megafon"
		}
		tag := amoTag.Tag{
			Name: operatorTag,
		}
		tags = append(tags, &tag)
	}

	leadsEmbeeded := &amoEmbeeded.Embedded{
		Contacts: []*amoContact.Contact{contact},
		Tags:     tags,
	}

	leadsCustomFields := []*customField.CustomField{}

	if len(lead.UtmMedium()) > 0 {
		utmMedium := &customField.CustomField{
			FieldId: 451717,
			Values: []customField.Enum{
				{
					Value: lead.UtmMedium(),
				},
			},
		}
		leadsCustomFields = append(leadsCustomFields, utmMedium)
	}

	if len(lead.UtmSource()) > 0 {

		if lead.IsJkFile() {
			lead.SetUtmSource(lead.UtmSource() + "_jk")
		}

		utmSource := &customField.CustomField{
			FieldId: 451715,
			Values: []customField.Enum{
				{
					Value: lead.UtmSource(),
				},
			},
		}
		leadsCustomFields = append(leadsCustomFields, utmSource)
	}

	if len(lead.UtmTerm()) > 0 {
		utmTerm := &customField.CustomField{
			FieldId: 416687,
			Values: []customField.Enum{
				{
					Value: lead.UtmTerm(),
				},
			},
		}
		leadsCustomFields = append(leadsCustomFields, utmTerm)
	}

	if len(lead.JkName()) > 0 {
		jkName := &customField.CustomField{
			FieldId: 416695,
			Values: []customField.Enum{
				{
					Value: lead.JkName(),
				},
			},
		}
		leadsCustomFields = append(leadsCustomFields, jkName)
	}

	cityName := ""
	for _, city := range cityList {
		if city.Id() == lead.CityId() {
			cityName = city.Name()
		}
	}
	if len(cityName) > 0 {
		cityName := &customField.CustomField{
			FieldId: 467915,
			Values: []customField.Enum{
				{
					Value: cityName,
				},
			},
		}
		leadsCustomFields = append(leadsCustomFields, cityName)
	}

	phoneCustomField := &customField.CustomField{
		FieldId: 632403,
		Values: []customField.Enum{
			{
				Value: lead.Phone(),
			},
		},
	}
	leadsCustomFields = append(leadsCustomFields, phoneCustomField)

	if lead.CityId() > 0 {
		cityId := &customField.CustomField{
			FieldId: 438603,
			Values: []customField.Enum{
				{
					Value: strconv.Itoa(lead.CityId()),
				},
			},
		}
		leadsCustomFields = append(leadsCustomFields, cityId)
	}

	if len(lead.JkName()) > 0 {
		jkName := &customField.CustomField{
			FieldId: 416695,
			Values: []customField.Enum{
				{
					Value: lead.JkName(),
				},
			},
		}
		leadsCustomFields = append(leadsCustomFields, jkName)
	}

	if len(lead.JkDesc()) > 0 {
		jkDesc := &customField.CustomField{
			FieldId: 629747,
			Values: []customField.Enum{
				{
					Value: lead.JkDesc(),
				},
			},
		}
		leadsCustomFields = append(leadsCustomFields, jkDesc)
	}

	if lead.JkId() > 0 {
		jkId := &customField.CustomField{
			FieldId: 417679,
			Values: []customField.Enum{
				{
					Value: strconv.Itoa(lead.JkId()),
				},
			},
		}
		leadsCustomFields = append(leadsCustomFields, jkId)
	}

	if len(lead.UtmCampaign()) > 0 {
		utmCampaign := &customField.CustomField{
			FieldId: 416685,
			Values: []customField.Enum{
				{
					Value: lead.UtmCampaign(),
				},
			},
		}
		leadsCustomFields = append(leadsCustomFields, utmCampaign)
	}

	leadR := &amoLead.Lead{
		Id:                leadIndex,
		Name:              "Сделка (leads.leadactiv.ru)",
		ResponsibleUserId: 2614462,
		StatusId:          lead.StatusId(),
		PipelineId:        lead.PipelineId(),
		CreatedBy:         2614462,
		Embedded:          leadsEmbeeded,
		CustomFileds:      leadsCustomFields,
	}
	return leadR
}
