package service

import (
	"amogo/internal/domain/lead"
	"amogo/pkg/amocrmClient"
	amoLead "amogo/pkg/amocrmClient/lead"
	"context"
	"errors"
	"fmt"
	"strconv"
	"sync"
	"time"

	"github.com/google/uuid"
)

func (ls *leadsService) SendLeadsKopilka(ctx context.Context, count uint64) (sendedCount int, err error) {
	leads, err := ls.repository.Lead.GetUnsendedLeads(ctx, lead.Kopilka, count)
	if err != nil {
		return
	}
	sentToUpdateInRepoInfo := map[uuid.UUID]int{}

	defer func(ls *leadsService) {
		for key, value := range sentToUpdateInRepoInfo {
			upFn := func(oldLead *lead.Lead) (*lead.Lead, error) {
				return lead.NewWithID(oldLead.Id(), oldLead.Phone(), oldLead.JkId(), oldLead.JkName(), oldLead.JkDesc(), oldLead.Operator(), oldLead.CityId(), oldLead.PipelineId(), oldLead.StatusId(), oldLead.SendType(), oldLead.IsShort(), oldLead.Tags(), oldLead.UtmMedium(), oldLead.UtmTerm(), oldLead.UtmSource(), oldLead.UtmCampaign(), oldLead.IsJkFile(), true, value, oldLead.CreatedBy(), oldLead.CreatedAt(), time.Now())
			}
			_, err := ls.repository.Lead.UpdateLead(ctx, key, upFn)
			if err != nil {
				ls.logger.Error("error updting lead with id %s, after send to kopilka", key.String())
			}
		}
	}(ls)

	for _, lead := range leads {
		id, err := ls.kopilka.SendLead(lead)
		if err != nil {
			return sendedCount, errors.New("error while sending to kopilka")
		}
		sentToUpdateInRepoInfo[lead.Id()] = id
	}
	sendedCount = len(sentToUpdateInRepoInfo)
	return
}

func (ls *leadsService) SendLeadsDirect(ctx context.Context, goroutinesCount int, leadsInPackageCount int) (sendedCount int, err error) {
	count := goroutinesCount * leadsInPackageCount
	leads, err := ls.repository.Lead.GetUnsendedLeads(ctx, lead.Direct, uint64(count))
	if err != nil {
		return
	}
	if len(leads) == 0 {
		return 0, nil
	}

	wg := sync.WaitGroup{}
	offset := 0
	for offset < len(leads) {
		end := offset + leadsInPackageCount
		if end > len(leads) {
			end = len(leads)
		}
		leadsList := leads[offset:end]
		wg.Add(1)
		go func(leads []*lead.Lead) {
			defer wg.Done()
			ls.doSendLeadsDirect(context.Background(), leads)
		}(leadsList)
		offset = end + 1
	}

	wg.Wait()
	return len(leads), nil
}

func (ls *leadsService) doSendLeadsDirect(ctx context.Context, leads []*lead.Lead) (sendedCount int, err error) {
	fmt.Println(len(leads))

	leadsPack := []*amoLead.Lead{}

	requestIdsMap := map[int]uuid.UUID{}
	sentToAmoUpdateInRepo := map[uuid.UUID]int{}

	defer func(ls *leadsService) {
		for key, value := range sentToAmoUpdateInRepo {
			upFn := func(oldLead *lead.Lead) (*lead.Lead, error) {
				return lead.NewWithID(oldLead.Id(), oldLead.Phone(), oldLead.JkId(), oldLead.JkName(), oldLead.JkDesc(), oldLead.Operator(), oldLead.CityId(), oldLead.PipelineId(), oldLead.StatusId(), oldLead.SendType(), oldLead.IsShort(), oldLead.Tags(), oldLead.UtmMedium(), oldLead.UtmTerm(), oldLead.UtmSource(), oldLead.UtmCampaign(), oldLead.IsJkFile(), true, value, oldLead.CreatedBy(), oldLead.CreatedAt(), time.Now())
			}
			_, err := ls.repository.Lead.UpdateLead(ctx, key, upFn)
			if err != nil {
				ls.logger.Error("error updting lead with id %s, after send to kopilka", key.String())
			}
		}
	}(ls)

	//list of cities
	cityList, err := ls.repository.City.ReadCityList(ctx)
	if err != nil {
		return
	}
	for index, lead := range leads {
		leadIns := ls.toAmoApiLead(index, lead, cityList)
		requestIdsMap[index] = lead.Id()
		leadsPack = append(leadsPack, leadIns)
	}

	result, err := ls.client.SendLeadComplex(amocrmClient.SendLeadComplexRequest{Leads: leadsPack})
	if err != nil {
		ls.logger.Error("error sendind leads to amo: %s", err.Error())
		return 0, err
	}

	leadsWitoutContacts := []int{}
	for _, resItem := range result.Result {
		requestId, err := strconv.Atoi(resItem.RequestId[0])
		if err != nil {
			continue
		}
		if resItem.ContactId == 0 {
			leadsWitoutContacts = append(leadsWitoutContacts, resItem.LeadId)
			continue
		}
		sentToAmoUpdateInRepo[requestIdsMap[requestId]] = resItem.LeadId
	}

	if len(leadsWitoutContacts) > 0 {
		//TODO - delete them
		ls.logger.Error("leads without contacts: %v", leadsWitoutContacts)
	}
	return
}
