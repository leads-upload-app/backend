package service

import (
	"amogo/internal/domain/lead"
	"amogo/pkg/gnApi"
	"context"
	"fmt"
	"strconv"
	"strings"
)

func (s *leadsService) parseFile(ctx context.Context, rows [][]string, params *CreateParams) (leads []*lead.Lead, err error) {
	switch params.Operator {
	case string(lead.Beeline):
		leads, err = s.processBeelineFile(ctx, rows, params)
		if err != nil {
			s.logger.Error("error processing beeline file: %s", err.Error())
			return nil, err
		}
	case string(lead.BeelineJK):
		leads, err = s.processBeelineJKFile(ctx, rows, params)
		if err != nil {
			s.logger.Error("error processing beeline jk file: %s", err.Error())
			return nil, err
		}
	case string(lead.Mts):
		leads, err = s.processMtsFile(ctx, rows, params)
		if err != nil {
			s.logger.Error("error processing mts file: %s", err.Error())
			return nil, err
		}
	case string(lead.Tele2):
		leads, err = s.processTele2File(ctx, rows, params)
		if err != nil {
			s.logger.Error("error processing tele2 file: %s", err.Error())
			return nil, err
		}
	case string(lead.Regular):
		leads, err = s.processRegularFile(ctx, rows, params)
		if err != nil {
			s.logger.Error("error processing regular file: %s", err.Error())
			return nil, err
		}
	default:
		leads, err = s.defaultFileParser(ctx, rows, params)
		if err != nil {
			s.logger.Error("error processing default file: %s", err.Error())
			return nil, err
		}
	}
	return
}

func (s *leadsService) defaultFileParser(ctx context.Context, rows [][]string, params *CreateParams) ([]*lead.Lead, error) {
	leads := []*lead.Lead{}
	for _, cells := range rows {
		if cells[0] == "" {
			continue
		}
		lead, err := lead.New(cells[0], params.JkId, params.JkName, "", lead.OperatorTypes(params.Operator), params.CityId, params.PipelineId, params.StatusId, lead.SendTypes(params.SendType), params.Short, params.Tags, params.UtmMedium, params.UtmTerm, params.UtmSource, params.UtmCampaign, params.IsJkFile, params.CreatedBy)
		if err != nil {
			return nil, fmt.Errorf("error creating lead : %s", err.Error())
		}
		leads = append(leads, lead)
	}
	return leads, nil
}

func (s *leadsService) processBeelineFile(ctx context.Context, rows [][]string, params *CreateParams) ([]*lead.Lead, error) {
	leads := []*lead.Lead{}
	for _, cells := range rows {
		if cells[0] == "" {
			continue
		}
		lead, err := lead.New(cells[0], params.JkId, params.JkName, "", lead.OperatorTypes(params.Operator), params.CityId, params.PipelineId, params.StatusId, lead.SendTypes(params.SendType), params.Short, params.Tags, params.UtmMedium, params.UtmTerm, params.UtmSource, params.UtmCampaign, params.IsJkFile, params.CreatedBy)
		if err != nil {
			return nil, fmt.Errorf("error creating lead : %s", err.Error())
		}
		leads = append(leads, lead)
	}
	return leads, nil
}

func (s *leadsService) processBeelineJKFile(ctx context.Context, rows [][]string, params *CreateParams) ([]*lead.Lead, error) {
	leads := []*lead.Lead{}
	jkInfoList, jkPrefixToIdMap, err := s.getJkInfoFromFileBeeline(ctx, rows)
	params.Operator = "beeline"
	if err != nil {
		return nil, fmt.Errorf("error getting jk info from portal: %s", err.Error())
	}
	for _, cells := range rows {
		if cells[0] == "" {
			continue
		}
		phone := cells[0]
		prefixString := string([]rune(phone)[0:3])
		if _, ok := jkPrefixToIdMap[prefixString]; !ok {
			return nil, fmt.Errorf("not found jk for prefix %s on portal", prefixString)
		}

		jkId, err := strconv.Atoi(jkPrefixToIdMap[prefixString])
		if err != nil {
			return nil, err
		}

		jkInfo, ok := jkInfoList[strconv.Itoa(jkId)]
		if !ok {
			return nil, fmt.Errorf("jk info not found on portal id in file: %d", jkId)
		}

		lead, err := lead.New(phone, jkId, jkInfo.Name, jkInfo.Description, lead.OperatorTypes(params.Operator), params.CityId, params.PipelineId, params.StatusId, lead.SendTypes(params.SendType), params.Short, []string{jkInfo.UtmSourceBeeline}, params.UtmMedium, params.UtmTerm, jkInfo.UtmSourceBeeline, params.UtmCampaign, params.IsJkFile, params.CreatedBy)
		if err != nil {
			return nil, fmt.Errorf("error creating lead : %s", err.Error())
		}
		leads = append(leads, lead)
	}
	return leads, nil
}

func (s *leadsService) processMtsFile(ctx context.Context, rows [][]string, params *CreateParams) ([]*lead.Lead, error) {
	leads := []*lead.Lead{}
	jkInfoList, err := s.getJkFromFileInfo(ctx, 0, rows)
	if err != nil {
		return nil, fmt.Errorf("error getting jk info from portal: %s", err.Error())
	}

	for _, cells := range rows {
		if cells[0] == "" {
			continue
		}
		values := strings.Split(cells[2], "-")
		min, err := strconv.Atoi(values[0])
		if err != nil {
			return nil, fmt.Errorf("error parsing min value in phones range: %s", err.Error())
		}
		max, err := strconv.Atoi(values[1])
		if err != nil {
			return nil, fmt.Errorf("error parsing max value in phones range: %s", err.Error())
		}

		jkId, err := strconv.Atoi(cells[0])
		if err != nil {
			return nil, err
		}

		jkInfo, ok := jkInfoList[cells[0]]
		if !ok {
			return nil, fmt.Errorf("jk info not found on portal id in file: %d", jkId)
		}

		for i := min; i <= max; i++ {

			phone := strconv.Itoa(i)

			for len([]rune(phone)) < 6 {
				phone = "0" + phone
			}

			phone = cells[1] + phone

			lead, err := lead.New(phone, jkId, jkInfo.Name, jkInfo.Description, lead.OperatorTypes(params.Operator), params.CityId, params.PipelineId, params.StatusId, lead.SendTypes(params.SendType), params.Short, []string{jkInfo.UtmSourceMts}, params.UtmMedium, params.UtmTerm, jkInfo.UtmSourceMts, params.UtmCampaign, params.IsJkFile, params.CreatedBy)
			if err != nil {
				return nil, fmt.Errorf("error creating lead : %s", err.Error())
			}
			leads = append(leads, lead)
		}

	}
	return leads, nil
}

func (s *leadsService) processTele2File(ctx context.Context, rows [][]string, params *CreateParams) ([]*lead.Lead, error) {
	leads := []*lead.Lead{}
	jkInfoList, err := s.getJkFromFileInfo(ctx, 1, rows)
	if err != nil {
		return nil, fmt.Errorf("error getting jk info from portal: %s", err.Error())
	}

	for _, cells := range rows {
		jkId, err := strconv.Atoi(cells[1])
		if err != nil {
			return nil, err
		}
		jkInfo, ok := jkInfoList[cells[1]]
		if !ok {
			return nil, fmt.Errorf("jk info not found on portal id in file: %d", jkId)
		}

		lead, err := lead.New(cells[0], jkId, jkInfo.Name, jkInfo.Description, lead.OperatorTypes(params.Operator), params.CityId, params.PipelineId, params.StatusId, lead.SendTypes(params.SendType), params.Short, []string{jkInfo.UtmSourceTele2}, params.UtmMedium, params.UtmTerm, jkInfo.UtmSourceTele2, params.UtmCampaign, params.IsJkFile, params.CreatedBy)
		if err != nil {
			return nil, fmt.Errorf("error creating lead : %s", err.Error())
		}
		leads = append(leads, lead)
	}
	return leads, nil
}

func (s *leadsService) processRegularFile(ctx context.Context, rows [][]string, params *CreateParams) ([]*lead.Lead, error) {
	leads := []*lead.Lead{}
	jkInfoList, err := s.getJkFromFileInfo(ctx, 1, rows)
	if err != nil {
		return nil, fmt.Errorf("error getting jk info from portal: %s", err.Error())
	}

	for _, cells := range rows {
		jkId, err := strconv.Atoi(cells[1])
		if err != nil {
			return nil, err
		}
		jkInfo, ok := jkInfoList[cells[1]]
		if !ok {
			return nil, fmt.Errorf("jk info not found on portal id in file: %d", jkId)
		}
		tag := cells[2] + jkInfo.UtmSourcePostav

		lead, err := lead.New(cells[0], jkId, jkInfo.Name, jkInfo.Description, lead.OperatorTypes(params.Operator), params.CityId, params.PipelineId, params.StatusId, lead.SendTypes(params.SendType), params.Short, []string{tag}, params.UtmMedium, params.UtmTerm, tag, params.UtmCampaign, params.IsJkFile, params.CreatedBy)
		if err != nil {
			return nil, fmt.Errorf("error creating lead : %s", err.Error())
		}
		leads = append(leads, lead)
	}
	return leads, nil
}

func (s *leadsService) getJkFromFileInfo(ctx context.Context, jkCellIndex int, fileRows [][]string) (map[string]gnApi.JkUtmInfo, error) {
	jkIdList, err := s.getUniqueJkListFromFile(jkCellIndex, fileRows)
	if err != nil {
		return nil, err
	}
	return s.api.GetJkParams(ctx, jkIdList)
}

func (s *leadsService) getUniqueJkListFromFile(jkCellIndex int, fileRows [][]string) (jkIdList []int, err error) {
	jkIdList = []int{}

	for _, cells := range fileRows {
		jkId, err := strconv.Atoi(cells[jkCellIndex])
		if err != nil {
			return nil, err
		}
		if !intInSlice(jkId, jkIdList) {
			jkIdList = append(jkIdList, jkId)
		}
	}

	return
}

func (s *leadsService) getJkInfoFromFileBeeline(ctx context.Context, rows [][]string) (map[string]gnApi.JkUtmInfo, map[string]string, error) {
	filePrefixesList := []int{}
	for _, cells := range rows {
		phone := cells[0]
		prefixString := string([]rune(phone)[0:3])
		prefix, err := strconv.Atoi(prefixString)
		if err != nil {
			return nil, nil, fmt.Errorf("error converting error while parsing prefix: %s", err.Error())
		}
		if !intInSlice(prefix, filePrefixesList) {
			filePrefixesList = append(filePrefixesList, prefix)
		}
	}

	prefixes, err := s.repository.BeelinePrefixes.ReadBeelinePrefixList(ctx)
	if err != nil {
		return nil, nil, fmt.Errorf("error reding prefixes from db: %s", err.Error())
	}

	jkIdsList := []int{}
	jkPrefixToIdMap := map[string]string{}

	for _, prefixFileItem := range filePrefixesList {
		jkPrefixToIdMap[strconv.Itoa(prefixFileItem)] = ""
		for _, prefix := range prefixes {
			if prefix.Prefix() == prefixFileItem {
				jkIdsList = append(jkIdsList, prefix.JkId())
				jkPrefixToIdMap[strconv.Itoa(prefixFileItem)] = strconv.Itoa(prefix.JkId())
				break
			}
		}
	}

	if len(jkIdsList) < len(filePrefixesList) {
		notDefined := []string{}
		for prefix, value := range jkPrefixToIdMap {
			if value == "" {
				notDefined = append(notDefined, fmt.Sprintf("prefix - %s: id jk - %s", prefix, value))
			}
		}
		return nil, nil, fmt.Errorf("check prefixes (no info): %v", notDefined)
	}

	jkInfos, err := s.api.GetJkParams(ctx, jkIdsList)
	if err != nil {
		return nil, nil, fmt.Errorf("error gettings jk infos from portal: %s", err.Error())
	}

	return jkInfos, jkPrefixToIdMap, nil
}
