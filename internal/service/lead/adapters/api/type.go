package api

import (
	"amogo/pkg/gnApi"
	"context"
)

type Api interface {
	GetJkParams(ctx context.Context, jkList []int) (jkParamsList map[string]gnApi.JkUtmInfo, err error)
}
