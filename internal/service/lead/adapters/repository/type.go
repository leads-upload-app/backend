package repository

import (
	"amogo/internal/domain/lead"
	"amogo/internal/domain/lead/filter"
	"context"

	"github.com/google/uuid"
)

type Repository interface {
	CreateLead(ctx context.Context, lead *lead.Lead) (err error)
	CreateLeadsList(ctx context.Context, leadsList []*lead.Lead) (err error)
	UpdateLead(ctx context.Context, updateFn func(lead *lead.Lead) (*lead.Lead, error)) (err error)
	DeleteLead(ctx context.Context, id uuid.UUID) (err error)
	ReadLeadsFiltredList(ctx context.Context, filter filter.Filter, limit, offset int) (leads []*lead.Lead, err error)
	GetUnsendedLeads(ctx context.Context, leadType lead.SendTypes, limit uint64) (leads []*lead.Lead, err error)
	GetUnsendedLeadsCount(ctx context.Context, sendType string) (count int, err error)
}
