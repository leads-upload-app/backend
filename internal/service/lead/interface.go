package service

import (
	"amogo/internal/domain/lead"
	"amogo/internal/domain/lead/filter"
	"context"
	"mime/multipart"
)

type Service interface {
	CrateLeadsList(ctx context.Context, params *CreateParams, reader *multipart.File) (leadsCount int, err error)
	GetLeadsList(ctx context.Context, filter filter.Filter) ([]*lead.Lead, error)
	SendLeadsKopilka(ctx context.Context, count uint64) (sendedCount int, err error)
	SendLeadsDirect(ctx context.Context, goroutinesCount int, leadsInPackageCount int) (sendedCount int, err error)
	GetUnsendedLeadsCount(ctx context.Context) (direct int, kopilka int, err error)
}
