package service

import (
	"amogo/internal/repository"
	"amogo/internal/service/lead/adapters/api"

	"amogo/pkg/amocrmClient"
	"amogo/pkg/kopilka"

	"gitlab.com/kanya384/gotools/logger"
)

type leadsService struct {
	client     *amocrmClient.Client
	api        api.Api
	repository *repository.Repository
	kopilka    *kopilka.Kopilka
	logger     logger.Interface
}

func NewLeadsService(repository *repository.Repository, client *amocrmClient.Client, api api.Api, logger logger.Interface) *leadsService {
	kopilka := kopilka.NewKopilka()
	return &leadsService{
		client:     client,
		repository: repository,
		kopilka:    kopilka,
		api:        api,
		logger:     logger,
	}
}
