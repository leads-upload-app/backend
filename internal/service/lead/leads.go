package service

import (
	"amogo/internal/domain/lead"
	"amogo/internal/domain/lead/filter"
	"amogo/pkg/excel"
	"context"
	"fmt"
	"mime/multipart"
)

const (
	defaultLeadsLimit = 100000000
)

func (s *leadsService) CrateLeadsList(ctx context.Context, params *CreateParams, reader *multipart.File) (leadsCount int, err error) {
	leadsList := []*lead.Lead{}
	for _, phone := range params.Phones {
		if phone == "" {
			continue
		}

		lead, err := lead.New(phone, params.JkId, params.JkName, params.JkDesc, lead.OperatorTypes(params.Operator), params.CityId, params.PipelineId, params.StatusId, lead.SendTypes(params.SendType), params.Short, params.Tags, params.UtmMedium, params.UtmTerm, params.UtmSource, params.UtmCampaign, params.IsJkFile, params.CreatedBy)
		if err != nil {
			s.logger.Error("error creating lead form list: %s", err.Error())
			return 0, fmt.Errorf("error creating lead : %s", err.Error())
		}
		leadsList = append(leadsList, lead)
	}

	if reader != nil {
		rows, err := excel.NewExcelService().ParseFile(*reader)
		if err != nil {
			return 0, err
		}

		insLeads, err := s.parseFile(ctx, rows, params)
		if err != nil {
			return 0, err
		}

		if insLeads != nil {
			leadsList = append(leadsList, insLeads...)
		}
	}
	err = s.repository.Lead.CreateLeadsList(context.Background(), leadsList)
	leadsCount = len(leadsList)
	return
}

func (ls *leadsService) GetLeadsList(ctx context.Context, filter filter.Filter) ([]*lead.Lead, error) {
	return ls.repository.Lead.ReadLeadsFiltredList(ctx, filter, defaultLeadsLimit, 0)
}

func (ls *leadsService) GetUnsendedLeadsCount(ctx context.Context) (direct int, kopilka int, err error) {
	direct, err = ls.repository.Lead.GetUnsendedLeadsCount(ctx, lead.Direct)
	if err != nil {
		return
	}
	kopilka, err = ls.repository.Lead.GetUnsendedLeadsCount(ctx, lead.Kopilka)
	if err != nil {
		return
	}
	return
}

func intInSlice(a int, list []int) bool {
	for _, b := range list {
		if b == a {
			return true
		}
	}
	return false
}
