package service

import (
	"amogo/internal/repository"
	amoStatus "amogo/internal/service/amoStatus"
	auth "amogo/internal/service/auth"
	beelinePrefix "amogo/internal/service/beelinePrefixes"
	city "amogo/internal/service/city"

	dmpLeads "amogo/internal/service/dmpLead"
	leads "amogo/internal/service/lead"
	"time"

	"amogo/pkg/amocrmClient"
	"amogo/pkg/gnApi"

	"gitlab.com/kanya384/gotools/logger"
)

const (
	tokenTTl = time.Hour * 2
)

type Service struct {
	City            city.Service
	AmoStatus       amoStatus.Service
	Auth            auth.Service
	BeelinePrefixes beelinePrefix.Service
	Leads           leads.Service
	DmpLeads        dmpLeads.Service
}

func NewService(repository *repository.Repository, tokenSecret string, client *amocrmClient.Client, passSalt string, logger logger.Interface, api *gnApi.GnApi) (*Service, error) {
	city, err := city.New(repository, logger, api, city.Options{})
	if err != nil {
		return nil, err
	}

	auth, err := auth.New(repository, tokenSecret, passSalt, logger, auth.Options{
		TokenTTL: tokenTTl,
	})
	if err != nil {
		return nil, err
	}

	beelinePrefix, err := beelinePrefix.New(repository, logger, beelinePrefix.Options{})
	if err != nil {
		return nil, err
	}

	amoStatus := amoStatus.New(repository, client, logger, amoStatus.Options{})

	leads := leads.NewLeadsService(repository, client, api, logger)
	dmpLeads := dmpLeads.NewLeadsService(repository, api, logger)

	return &Service{
		City:            city,
		AmoStatus:       amoStatus,
		Auth:            auth,
		BeelinePrefixes: beelinePrefix,
		Leads:           leads,
		DmpLeads:        dmpLeads,
	}, nil
}
