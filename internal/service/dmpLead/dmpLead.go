package service

import (
	"amogo/internal/domain/dmpLead"
	"amogo/internal/domain/dmpLead/filter"
	"context"
	"strconv"
)

const (
	defaultLeadsLimit = 100000000
)

func (s *service) CreateLead(ctx context.Context, dmpLead *dmpLead.DmpLead) (err error) {
	list, err := s.api.GetJkParams(ctx, []int{dmpLead.JkId()})

	params, ok := list[strconv.Itoa(dmpLead.JkId())]
	if !ok {
		s.logger.Error("error creating dmp.lead, not found jk with id %d on portal : %s", dmpLead.JkId(), err.Error())
		return
	}
	dmpLead.SetUtmSource("dmp" + params.UtmSourcePostav + "_jk")
	dmpLead.SetTag("dmp" + params.UtmSourcePostav + "_jk")
	dmpLead.SetJkName(params.Name)
	dmpLead.SetCityId(params.SId)

	err = s.repository.CreateLead(ctx, dmpLead)

	if err != nil {
		s.logger.Error("error creating dmp.lead: %s", err.Error())
		return
	}
	return
}

func (s *service) ReadLeadsFiltredList(ctx context.Context, filter filter.Filter) (leads []*dmpLead.DmpLead, err error) {
	leads, err = s.repository.ReadLeadsFiltredList(ctx, filter, defaultLeadsLimit, 0)
	if err != nil {
		s.logger.Error("error reading dmp.leads: %s", err.Error())
		return
	}
	return
}
