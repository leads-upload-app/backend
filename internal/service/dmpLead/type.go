package service

import (
	repo "amogo/internal/repository"
	"amogo/internal/service/dmpLead/adapters/repository"
	"amogo/internal/service/lead/adapters/api"
	"amogo/pkg/kopilka"

	"gitlab.com/kanya384/gotools/logger"
)

type service struct {
	repository repository.Repository
	kopilka    *kopilka.Kopilka
	api        api.Api
	logger     logger.Interface
}

func NewLeadsService(repository *repo.Repository, api api.Api, logger logger.Interface) *service {
	kopilka := kopilka.NewKopilka()
	return &service{
		repository: repository.DmpLead,
		kopilka:    kopilka,
		api:        api,
		logger:     logger,
	}
}
