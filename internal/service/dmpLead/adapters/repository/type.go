package repository

import (
	"amogo/internal/domain/dmpLead"
	"amogo/internal/domain/dmpLead/filter"
	"context"

	"github.com/google/uuid"
)

type Repository interface {
	CreateLead(ctx context.Context, dmpLead *dmpLead.DmpLead) (err error)
	UpdateLead(ctx context.Context, Id uuid.UUID, updateFn func(lead *dmpLead.DmpLead) (*dmpLead.DmpLead, error)) (lead *dmpLead.DmpLead, err error)
	GetUnsendedLeads(ctx context.Context, limit uint64) (leads []*dmpLead.DmpLead, err error)
	ReadLeadsFiltredList(ctx context.Context, filter filter.Filter, limit, offset int) (leads []*dmpLead.DmpLead, err error)
}
