package service

import (
	"amogo/internal/domain/dmpLead"
	"context"
	"fmt"
	"time"

	"github.com/google/uuid"
)

func (ls *service) SendDmpLeadsToKopilka(ctx context.Context, count uint64) (sendedCount int, err error) {
	leads, err := ls.repository.GetUnsendedLeads(ctx, count)
	if err != nil {
		return
	}
	sentToUpdateInRepoInfo := map[uuid.UUID]int{}

	defer func(ls *service) {
		for key, value := range sentToUpdateInRepoInfo {
			upFn := func(oldLead *dmpLead.DmpLead) (*dmpLead.DmpLead, error) {
				return dmpLead.NewWithId(oldLead.Id(), oldLead.Phone(), oldLead.Site(), oldLead.SiteWithParams(), oldLead.Email(), oldLead.Tag(), oldLead.UtmMedium(), oldLead.UtmTerm(), oldLead.UtmSource(), oldLead.UtmCampaign(), oldLead.UtmContent(), oldLead.Roistat(), oldLead.Yid(), oldLead.JkId(), oldLead.JkName(), oldLead.CityId(), true, value, oldLead.CreatedAt(), time.Now())
			}
			_, err := ls.repository.UpdateLead(ctx, key, upFn)
			if err != nil {
				ls.logger.Error("error updting lead with id %s, after send to kopilka", key.String())
			}
		}
	}(ls)

	for _, lead := range leads {
		id, err := ls.kopilka.SendDmpOneLead(lead)
		if err != nil {
			return sendedCount, fmt.Errorf("error while sending to kopilka: %s", err.Error())
		}
		sentToUpdateInRepoInfo[lead.Id()] = id
	}
	sendedCount = len(sentToUpdateInRepoInfo)
	return
}
