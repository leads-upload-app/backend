package service

import (
	"amogo/internal/domain/dmpLead"
	"amogo/internal/domain/dmpLead/filter"
	"context"
)

type Service interface {
	CreateLead(ctx context.Context, dmpLead *dmpLead.DmpLead) (err error)
	SendDmpLeadsToKopilka(ctx context.Context, count uint64) (sendedCount int, err error)
	ReadLeadsFiltredList(ctx context.Context, filter filter.Filter) (leads []*dmpLead.DmpLead, err error)
}
