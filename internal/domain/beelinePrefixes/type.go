package beelinePrefix

import (
	"time"

	"github.com/google/uuid"
)

type BeelinePrefix struct {
	id         uuid.UUID
	prefix     int
	jkId       int
	createdAt  time.Time
	modifiedAt time.Time
}

func NewWithId(
	id uuid.UUID,
	prefix int,
	jkId int,
	createdAt time.Time,
	modifiedAt time.Time,
) (*BeelinePrefix, error) {
	return &BeelinePrefix{
		id:         id,
		prefix:     prefix,
		jkId:       jkId,
		createdAt:  createdAt,
		modifiedAt: modifiedAt,
	}, nil
}

func New(
	prefix int,
	jkId int,
) (*BeelinePrefix, error) {
	return &BeelinePrefix{
		id:         uuid.New(),
		prefix:     prefix,
		jkId:       jkId,
		createdAt:  time.Now(),
		modifiedAt: time.Now(),
	}, nil
}

func (b BeelinePrefix) Id() uuid.UUID {
	return b.id
}

func (b BeelinePrefix) Prefix() int {
	return b.prefix
}

func (b BeelinePrefix) JkId() int {
	return b.jkId
}

func (b BeelinePrefix) CreatedAt() time.Time {
	return b.createdAt
}

func (b BeelinePrefix) ModifiedAt() time.Time {
	return b.modifiedAt
}
