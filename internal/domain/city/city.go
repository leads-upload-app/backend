package city

import (
	"time"
)

type City struct {
	id         int
	name       string
	createdAt  time.Time
	modifiedAt time.Time
}

func New(
	id int,
	name string,
) (*City, error) {
	return &City{
		id:         id,
		name:       name,
		createdAt:  time.Now(),
		modifiedAt: time.Now(),
	}, nil
}

func NewWithId(
	id int,
	name string,
	createdAt time.Time,
	modifiedAt time.Time,
) (*City, error) {
	return &City{
		id:         id,
		name:       name,
		createdAt:  createdAt,
		modifiedAt: modifiedAt,
	}, nil
}

func (c City) Id() int {
	return c.id
}

func (c City) Name() string {
	return c.name
}

func (c City) CreatedAt() time.Time {
	return c.createdAt
}

func (c City) ModifiedAt() time.Time {
	return c.modifiedAt
}
