package filter

import (
	"time"
)

type Filter struct {
	DateStart time.Time
	DateEnd   time.Time
	JkId      int
}
