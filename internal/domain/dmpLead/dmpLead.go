package dmpLead

import (
	"time"

	"github.com/google/uuid"
)

type DmpLead struct {
	id             uuid.UUID
	phone          string
	site           string
	siteWithParams string
	email          string
	tag            string
	utmMedium      string
	utmTerm        string
	utmSource      string
	utmCampaign    string
	utmContent     string
	roistat        string
	processed      bool
	responseId     int
	yId            string
	jkId           int
	jkName         string
	cityId         string
	createdAt      time.Time
	modifiedAt     time.Time
}

func NewWithId(
	id uuid.UUID,
	phone string,
	site string,
	siteWithParams string,
	email string,
	tag string,
	utmMedium string,
	utmTerm string,
	utmSource string,
	utmCampaign string,
	utmContent string,
	roistat string,
	yId string,
	jkId int,
	jkName string,
	cityId string,
	processed bool,
	responseId int,
	createdAt time.Time,
	modifiedAt time.Time,
) (*DmpLead, error) {
	return &DmpLead{
		id:             id,
		phone:          phone,
		site:           site,
		siteWithParams: siteWithParams,
		email:          email,
		tag:            tag,
		utmMedium:      utmMedium,
		utmTerm:        utmTerm,
		utmSource:      utmSource,
		utmCampaign:    utmCampaign,
		utmContent:     utmContent,
		roistat:        roistat,
		yId:            yId,
		jkId:           jkId,
		jkName:         jkName,
		cityId:         cityId,
		processed:      processed,
		responseId:     responseId,
		createdAt:      createdAt,
		modifiedAt:     modifiedAt,
	}, nil
}

func New(
	phone string,
	site string,
	siteWithParams string,
	email string,
	tag string,
	utmMedium string,
	utmTerm string,
	utmSource string,
	utmCampaign string,
	utmContent string,
	roistat string,
	yId string,
	jkId int,
	jkName string,
	cityId string,
) (*DmpLead, error) {
	return &DmpLead{
		id:             uuid.New(),
		phone:          phone,
		site:           site,
		siteWithParams: siteWithParams,
		email:          email,
		tag:            tag,
		utmMedium:      utmMedium,
		utmTerm:        utmTerm,
		utmSource:      utmSource,
		utmCampaign:    utmCampaign,
		utmContent:     utmContent,
		roistat:        roistat,
		yId:            yId,
		jkId:           jkId,
		jkName:         jkName,
		cityId:         cityId,
		processed:      false,
		responseId:     0,
		createdAt:      time.Now(),
		modifiedAt:     time.Now(),
	}, nil
}

func (l DmpLead) Id() uuid.UUID {
	return l.id
}

func (l DmpLead) Phone() string {
	return l.phone
}

func (l DmpLead) Site() string {
	return l.site
}

func (l DmpLead) SiteWithParams() string {
	return l.siteWithParams
}

func (l DmpLead) Email() string {
	return l.email
}

func (l DmpLead) Tag() string {
	return l.tag
}

func (l *DmpLead) SetTag(tag string) {
	l.tag = tag
}

func (l DmpLead) UtmMedium() string {
	return l.utmMedium
}

func (l DmpLead) UtmTerm() string {
	return l.utmTerm
}

func (l DmpLead) UtmSource() string {
	return l.utmSource
}

func (l *DmpLead) SetUtmSource(utmSource string) {
	l.utmSource = utmSource
}

func (l DmpLead) UtmCampaign() string {
	return l.utmCampaign
}

func (l DmpLead) UtmContent() string {
	return l.utmContent
}

func (l DmpLead) Roistat() string {
	return l.roistat
}

func (l DmpLead) Yid() string {
	return l.yId
}

func (l DmpLead) JkId() int {
	return l.jkId
}

func (l DmpLead) JkName() string {
	return l.jkName
}

func (l *DmpLead) SetJkName(jkName string) {
	l.jkName = jkName
}

func (l DmpLead) Processed() bool {
	return l.processed
}

func (l *DmpLead) SetProcessed(processed bool) {
	l.processed = processed
}

func (l DmpLead) ResponseId() int {
	return l.responseId
}

func (l *DmpLead) SetResponseId(responseId int) {
	l.responseId = responseId
}

func (l DmpLead) CreatedAt() time.Time {
	return l.createdAt
}

func (l DmpLead) ModifiedAt() time.Time {
	return l.modifiedAt
}

func (l DmpLead) CityId() string {
	return l.cityId
}

func (l *DmpLead) SetCityId(cityId string) {
	l.cityId = cityId
}
