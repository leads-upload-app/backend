package user

import (
	"time"

	"github.com/google/uuid"
)

type User struct {
	id         uuid.UUID
	name       string
	login      string
	pass       string
	createdAt  time.Time
	modifiedAt time.Time
}

func NewWithID(
	id uuid.UUID,
	name string,
	login string,
	pass string,
	createdAt time.Time,
	modifiedAt time.Time,
) (*User, error) {
	return &User{
		id:         id,
		name:       name,
		login:      login,
		pass:       pass,
		createdAt:  createdAt,
		modifiedAt: modifiedAt,
	}, nil
}

func New(
	name string,
	login string,
	pass string,
) (*User, error) {
	var timeNow = time.Now().UTC()
	return &User{
		id:         uuid.New(),
		name:       name,
		login:      login,
		pass:       pass,
		createdAt:  timeNow,
		modifiedAt: timeNow,
	}, nil
}

func (c User) Id() uuid.UUID {
	return c.id
}

func (c User) Name() string {
	return c.name
}

func (c User) Login() string {
	return c.login
}

func (c User) Pass() string {
	return c.pass
}

func (c *User) SetPass(pass string) {
	c.pass = pass
}

func (c User) CreatedAt() time.Time {
	return c.createdAt
}

func (c User) ModifiedAt() time.Time {
	return c.modifiedAt
}
