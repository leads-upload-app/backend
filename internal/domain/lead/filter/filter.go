package filter

import (
	"amogo/internal/domain/lead"
	"time"

	"github.com/google/uuid"
)

type Filter struct {
	DateStart   time.Time
	DateEnd     time.Time
	SendType    lead.SendTypes
	Tag         string
	UtmMedium   string
	UtmTerm     string
	UtmSource   string
	UtmCampaign string
	CreatedBy   uuid.UUID
}
