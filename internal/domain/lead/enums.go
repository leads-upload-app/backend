package lead

type OperatorTypes string

const (
	Beeline   OperatorTypes = "beeline"
	BeelineJK OperatorTypes = "beelineJK"
	Megafon   OperatorTypes = "megafon"
	Mts       OperatorTypes = "mts"
	Tele2     OperatorTypes = "tele2"
	Regular   OperatorTypes = "regular"
)

type SendTypes string

const (
	Direct  SendTypes = "direct"
	Kopilka SendTypes = "kopilka"
)
