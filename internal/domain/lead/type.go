package lead

import (
	"time"

	"github.com/google/uuid"
)

type Lead struct {
	id          uuid.UUID
	phone       string
	jkId        int
	jkName      string
	jkDesc      string
	operator    OperatorTypes
	cityId      int
	pipelineId  int
	statusId    int
	sendType    SendTypes
	short       bool
	tags        []string
	utmMedium   string
	utmTerm     string
	utmSource   string
	utmCampaign string
	isJkFile    bool
	processed   bool
	responseId  int
	createdBy   uuid.UUID
	createdAt   time.Time
	modifiedAt  time.Time
}

func NewWithID(
	id uuid.UUID,
	phone string,
	jkId int,
	jkName string,
	jkDesc string,
	operator OperatorTypes,
	cityId int,
	pipelineId int,
	statusId int,
	sendType SendTypes,
	short bool,
	tags []string,
	utmMedium string,
	utmTerm string,
	utmSource string,
	utmCampaign string,
	isJkFile bool,
	processed bool,
	responseId int,
	createdBy uuid.UUID,
	createdAt time.Time,
	modifiedAt time.Time,
) (*Lead, error) {
	return &Lead{
		id:          id,
		phone:       phone,
		jkId:        jkId,
		jkName:      jkName,
		jkDesc:      jkDesc,
		operator:    operator,
		cityId:      cityId,
		pipelineId:  pipelineId,
		statusId:    statusId,
		sendType:    sendType,
		short:       short,
		tags:        tags,
		utmMedium:   utmMedium,
		utmTerm:     utmTerm,
		utmSource:   utmSource,
		utmCampaign: utmCampaign,
		isJkFile:    isJkFile,
		processed:   processed,
		responseId:  responseId,
		createdBy:   createdBy,
		createdAt:   createdAt,
		modifiedAt:  modifiedAt,
	}, nil
}

func New(
	phone string,
	jkId int,
	jkName string,
	jkDesc string,
	operator OperatorTypes,
	cityId int,
	pipelineId int,
	statusId int,
	sendType SendTypes,
	short bool,
	tags []string,
	utmMedium string,
	utmTerm string,
	utmSource string,
	utmCampaign string,
	isJkFile bool,
	createdBy uuid.UUID,
) (*Lead, error) {
	return &Lead{
		id:          uuid.New(),
		phone:       phone,
		jkId:        jkId,
		jkName:      jkName,
		jkDesc:      jkDesc,
		operator:    operator,
		cityId:      cityId,
		pipelineId:  pipelineId,
		statusId:    statusId,
		sendType:    sendType,
		short:       short,
		tags:        tags,
		utmMedium:   utmMedium,
		utmTerm:     utmTerm,
		utmSource:   utmSource,
		utmCampaign: utmCampaign,
		isJkFile:    isJkFile,
		processed:   false,
		responseId:  0,
		createdBy:   createdBy,
		createdAt:   time.Now(),
		modifiedAt:  time.Now(),
	}, nil
}

func (l Lead) Id() uuid.UUID {
	return l.id
}

func (l Lead) IsJkFile() bool {
	return l.isJkFile
}

func (l Lead) Phone() string {
	return l.phone
}

func (l Lead) JkId() int {
	return l.jkId
}

func (l Lead) JkName() string {
	return l.jkName
}

func (l Lead) JkDesc() string {
	return l.jkDesc
}

func (l Lead) Operator() OperatorTypes {
	return l.operator
}

func (l Lead) CityId() int {
	return l.cityId
}

func (l Lead) PipelineId() int {
	return l.pipelineId
}

func (l Lead) StatusId() int {
	return l.statusId
}

func (l Lead) SendType() SendTypes {
	return l.sendType
}

func (l Lead) IsShort() bool {
	return l.short
}

func (l Lead) Tags() []string {
	return l.tags
}

func (l Lead) UtmMedium() string {
	return l.utmMedium
}

func (l Lead) UtmTerm() string {
	return l.utmTerm
}

func (l Lead) UtmSource() string {
	return l.utmSource
}

func (l *Lead) SetUtmSource(utmSource string) {
	l.utmSource = utmSource
}

func (l Lead) UtmCampaign() string {
	return l.utmCampaign
}

func (l Lead) Processed() bool {
	return l.processed
}

func (l Lead) ResponseId() int {
	return l.responseId
}

func (l Lead) CreatedBy() uuid.UUID {
	return l.createdBy
}

func (l Lead) CreatedAt() time.Time {
	return l.createdAt
}

func (l Lead) ModifiedAt() time.Time {
	return l.modifiedAt
}
