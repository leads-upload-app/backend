package amoStatus

import (
	"time"

	"github.com/google/uuid"
)

type AmoStatus struct {
	id           uuid.UUID
	pipelineName string
	pipelineId   int
	statusName   string
	statusId     int
	createdAt    time.Time
	modifiedAt   time.Time
}

func (c AmoStatus) Id() uuid.UUID {
	return c.id
}

func (c AmoStatus) PipelineName() string {
	return c.pipelineName
}

func (c AmoStatus) PipelineId() int {
	return c.pipelineId
}

func (c AmoStatus) StatusName() string {
	return c.statusName
}

func (c AmoStatus) StatusId() int {
	return c.statusId
}

func (c AmoStatus) CreatedAt() time.Time {
	return c.createdAt
}

func (c AmoStatus) ModifiedAt() time.Time {
	return c.modifiedAt
}

func NewWithId(
	id uuid.UUID,
	pipelineName string,
	pipelineId int,
	statusName string,
	statusId int,
	createdAt time.Time,
	modifiedAt time.Time,
) (*AmoStatus, error) {
	return &AmoStatus{
		id:           id,
		pipelineName: pipelineName,
		pipelineId:   pipelineId,
		statusName:   statusName,
		statusId:     statusId,
		createdAt:    createdAt,
		modifiedAt:   modifiedAt,
	}, nil
}

func New(
	pipelineName string,
	pipelineId int,
	statusName string,
	statusId int,
) (*AmoStatus, error) {
	return &AmoStatus{
		id:           uuid.New(),
		pipelineName: pipelineName,
		pipelineId:   pipelineId,
		statusName:   statusName,
		statusId:     statusId,
		createdAt:    time.Now(),
		modifiedAt:   time.Now(),
	}, nil
}
